using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

/// <summary>
/// Script Template: Unity Technologies
/// Editor: Michael DiLucca
/// As of 10/27/2016 This script will be refactored and duplicated to a 3D version
/// leaving this version as a referance, and if needed, a go back.
/// We plan to switch to a 3D engine to run our 2D game for collision reasons.
/// In order to do this we start by changing the player controller to 3D
/// PLEASE SEE: Platformer3DUserControl AS THIS SCRIPT IS CURRENTLY OBSOLETE!!!
/// </summary>

[RequireComponent(typeof(PlatformerCharacter2D))]
public class Platformer2DUserControl : MonoBehaviour {
    private PlatformerCharacter2D m_Character;
    private bool m_Jump;

    // Move into a background structure and manipulate the colliders accordingly
    //private ActivateBackCollision abc;
    private bool canEnter = true;
    private bool canInteract = false;

    private void Awake() {
        m_Character = GetComponent<PlatformerCharacter2D>();
    }


    private void Update() {
        if (!m_Jump) {
            // Read the jump input in Update so button presses aren't missed.
            m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
        }

        if (canInteract) {
            if (Input.GetButtonDown("Enter") && canEnter) {
                canEnter = false;
                transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, 1.5f);
                //abc.ToggleColliders(true);
                print("entered");
            }

            if (Input.GetButtonDown("Exit") && !canEnter) {
                canEnter = true;
                transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, -1);
                //abc.ToggleColliders(false);
                print("exited");
            }
        }
    }


    private void FixedUpdate() {
        // Read the inputs.
        float h = CrossPlatformInputManager.GetAxis("Horizontal");
        float v = CrossPlatformInputManager.GetAxis("Vertical");
        // Pass all parameters to the character control script.
        m_Character.Move(h, v, m_Jump);
        m_Jump = false;
    }

    void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "TriggerZone" && !canInteract && tag != "Limb") {
            //abc = other.GetComponent<ActivateBackCollision>();
            print("canInteract");
            canInteract = true;
        }
    }

    void OnTriggerExit2D(Collider2D other) {
        //abc = null;
        print("can NOT Interact");
        canInteract = false;
    }
}

