﻿using UnityEngine;
using System.Collections;

public class ReviveAllyAIPackage : AIPackage
{
	public EnemyManager target;
	public float moveSpeed;
	public float threshold = 0.5f;

    private bool targetReached = false;

	public override void StartPackage()
	{
		if (agent != null)
		{
			if (target != null)
			{
				// Temporary flip function until we implement proper animations.
				if (manager != null)
				{
					float diff = target.transform.position.x - agent.transform.position.x;

					if (diff > 0)
						manager.Turn(false);
					else if (diff < 0)
						manager.Turn(true);
				}

				agent.destination = new Vector3(target.transform.position.x, target.transform.position.y, manager.HardZ);

                manager.leftVisionField.SetActive(false);
                manager.rightVisionField.SetActive(false);

				if (moveSpeed > 0)
					agent.speed = moveSpeed;
				else
					Debug.Log("Move speed was not set properly in ReviveAllyAIPackage. Defaulting to current agent speed.");

				this.enabled = true;
			}
			else
			{
				Debug.Log("Target was not set in ReviveAllyAIPackage. Ending patrol.");
				EndPackage();
				this.enabled = false;
			}

		}
		else
		{
			Debug.Log("Agent was not attached properly to ReviveAllyAIPackage. Ending patrol.");
			EndPackage();
			this.enabled = false;
		}
	}

	void Update()
	{
		if (!targetReached && agent != null && target != null)
		{
			// For the case that we were given a moving target.
			agent.destination = target.transform.position;

			// Temporary flip function until we implement proper animations.
			if (manager != null)
			{
				float diff = target.transform.position.x - agent.transform.position.x;

				if (diff > 0)
					manager.Turn(false);
				else if (diff < 0)
					manager.Turn(true);
			}

			// This patrol will end when the agent has reached its destination, checked for here.
			if (!agent.pathPending)
			{
				if (agent.remainingDistance <= threshold || agent.remainingDistance <= agent.stoppingDistance)
				{
					//if (!agent.hasPath || agent.velocity.sqrMagnitude == 0f)
					{
                        targetReached = true;
                        //agent.Stop();
                        agent.destination = transform.position;
                        Debug.Log("Heyoooo");

                        StartCoroutine(WakeEmUpBoiCoroutine());
					}
				}
			}
		}
	}

    private IEnumerator WakeEmUpBoiCoroutine()
    {
        if (manager != null && manager.anim != null && manager.wakeUpOtherClip != null)
        {
            Debug.Log("wOO");

            manager.anim.SetTrigger("wakeUpOther");

            float halfLength = (manager.wakeUpOtherClip.length / 3f) / 2f;

            yield return new WaitForSeconds(halfLength);

            target.RecoverFromStun();

            yield return new WaitForSeconds(halfLength);

            EndPackage();
            //Debug.Log("Done moving.");
            this.enabled = false;
        }
    }

	public override void LogInfo()
	{
		Debug.Log("LogInfo: This is a Revive Ally AI Package.\n" +
			"Target: " + target + "\n" +
			"MoveSpeed: " + moveSpeed + "\n" +
			"Threshold: " + threshold);
	}
}
