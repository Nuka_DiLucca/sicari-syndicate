﻿using System.Collections;
using UnityEngine;


public enum HackType { None, ID, Disguise }


public class Door : HackableObject
{
    #region Variables
    private Rigidbody rb;

    public bool opensToTheLeft = false;
    public HackType type;
    public float openSpeed = 20;
	public float moveTo = -0.42f;
    #endregion

    #region Functions
    private IEnumerator Open()
    {
		float positionThisFrame = transform.position.z;

        while (positionThisFrame < moveTo) {
			positionThisFrame = transform.position.z + Time.deltaTime;
			transform.position = new Vector3(transform.position.x, transform.position.y, positionThisFrame);
            yield return new WaitForEndOfFrame();
        }
    }
    #endregion

    #region MonoBehaviours
    private void Awake()
	{
		rb = GetComponent<Rigidbody>();
	}

    #endregion

	public override void Hack()
	{
		sound.Play ();
		StartCoroutine(Open());
	}
}