﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;


/// <summary>
/// Temporary package, just so that something happens when a guard catches up with Kaida in our test build.
/// </summary>
public class CatchKaidaAIPackage : AIPackage
{
	public override void StartPackage()
	{
		if (EnemyObserver.kaida != null)
			StartCoroutine(DoCatch());
	}

	private IEnumerator DoCatch()
	{
		Debug.Log("Caught.");

		if (EnemyObserver.kaida.loss != null)
			EnemyObserver.kaida.loss.enabled = true;
		else
			Debug.Log("No loss image found in stored Platform3DUserControl.");
		
		EnemyObserver.kaida.enabled = false;

		Time.timeScale = 0f;
		float targetTime = Time.realtimeSinceStartup + 4f;

		while (Time.realtimeSinceStartup < targetTime)
		{
			yield return 0;
		}

		Time.timeScale = 1f;

		foreach (EnemyManager enemy in EnemyObserver.enemies)
			enemy.Unsubscribe();
		EnemyObserver.enemies.Clear();

		SceneManager.LoadScene(SceneManager.GetActiveScene().name);

		if (EnemyObserver.kaida.loss != null)
			EnemyObserver.kaida.loss.enabled = false;
		else
			Debug.Log("No loss image found in stored Platform3DUserControl.");
		
		EndPackage();
	}

	public override void LogInfo()
	{
		Debug.Log("LogInfo: This is a Catch Kaida AI Package.");
	}
}
