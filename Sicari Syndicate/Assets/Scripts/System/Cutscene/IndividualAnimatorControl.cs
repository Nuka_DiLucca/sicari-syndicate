﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class IndividualAnimatorControl : MonoBehaviour
{
    private Animator _anim;
    //public int animationID = 0;
    public bool isFacingRight = false;
    [SerializeField] private bool isKaida = false;
    public bool isHandless = false;

    private void Awake()
    {
        _anim = GetComponent<Animator>();
    }

    private void Update()
    {
        //_anim.SetInteger("animationID", animationID);

        if (isFacingRight)
            _anim.SetFloat("facingRight", 1);
        else
            _anim.SetFloat("facingRight", 0);

        if (isKaida)
        {
            if (isHandless)
                _anim.SetFloat("handless", 1);
            else
                _anim.SetFloat("handless", 0);
        }
    }
}