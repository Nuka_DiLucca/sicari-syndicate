﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyObserver : MonoBehaviour
{
	public static Platformer3DUserControlKaida kaida; // Will not be present in final build.
	public static List<EnemyManager> enemies = new List<EnemyManager>();

	public delegate void AlertEventHandler(EnemyManager origin);
	public static event AlertEventHandler AlertAllOtherEnemies;

	public static void RaiseAlertAllOtherEnemies(EnemyManager alertOrigin)
	{
		if (AlertAllOtherEnemies != null)
			AlertAllOtherEnemies(alertOrigin);
	}
}
