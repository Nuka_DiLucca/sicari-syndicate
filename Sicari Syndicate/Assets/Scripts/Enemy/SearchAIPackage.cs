﻿using UnityEngine;
using System.Collections;

public class SearchAIPackage : AIPackage
{
	public Transform initialTarget;
	public int minNumPatrols = 4;
	public int maxNumPatrols = 8;
	public float maxSearchDistance = 5f;

	public override void StartPackage()
	{
		if (initialTarget != null && manager != null && manager.CurrentSequence == manager.normalPatrols && agent != null)
		{
			int numPatrols;
			if (minNumPatrols >/*=*/ 0 && maxNumPatrols > 0 && maxNumPatrols >= minNumPatrols)
			{
				//numPatrols = Random.Range(minNumPatrols, (maxNumPatrols + 1));
				numPatrols = Random.Range((minNumPatrols / 2), ((maxNumPatrols + 1) / 2)) * 2;
			}
			else
			{
				//numPatrols = 3;
				numPatrols = 4;
			}
			
			AIPackage[] searchSequence = new AIPackage[numPatrols];

			// The first package, if any, must be a move package, or else we may have a guard who just sits in place when they're suspicious.
			if (numPatrols >= 1)
			{
				searchSequence[0] = MakeMovePackage();
			}

			//int coinflip;
			bool nextIsWait = false;
			for (int i = 1; i < searchSequence.Length; i++)
			{
				//coinflip = Random.Range(0, 2);
				nextIsWait = !nextIsWait;

				//if (coinflip == 0)
				if (!nextIsWait)
				{
					// Make the next package a move package.
					searchSequence[i] = MakeMovePackage();
				}
				else
				{
					// Make the next package a wait package.
					searchSequence[i] = MakeWaitPackage();
				}
			}

			manager.searchPatrols = searchSequence;
			if (!manager.EnterPatrolSequence(manager.searchPatrols, 0))
				EndPackage();
		}
	}

	private MoveAIPackage MakeMovePackage()
	{
		MoveAIPackage newPackage = manager.gameObject.AddComponent<MoveAIPackage>();
		newPackage.AttachManager(manager);
		newPackage.AttachAgent(agent);
		newPackage.moveSpeed = 2f;

		if (initialTarget != null)
		{
			int attempts = 10;
			float x;
			float diff = Mathf.Abs(maxSearchDistance);
			Vector3 newPos;
			NavMeshPath testPath = new NavMeshPath();

			while (attempts > 0)
			{
				x = Random.Range(initialTarget.position.x - diff, initialTarget.position.x + diff);

				newPos = new Vector3(x, manager.transform.position.y, manager.transform.position.z);

				if (agent.CalculatePath(newPos, testPath))
				{
					Transform newTarget = (new GameObject()).transform;
					newTarget.position = newPos;
					newPackage.target = newTarget;

					newTarget.gameObject.name = "Search Target " + newPos.x;

					break;
				}

				attempts--;
			}

			// No good target could be found after the given number of attempts.
			if (attempts == 0)
			{
				newPackage.target = manager.transform;
			}
		}
		else
		{
			newPackage.target = manager.transform;
		}

		//Debug.Log("Creating move package targeting " + newPackage.target + " at an X of " + newPackage.target.position.x + ", where MSD: " + maxSearchDistance + ".");

		newPackage.enabled = false;
		return newPackage;
	}

	private WaitAIPackage MakeWaitPackage()
	{
		WaitAIPackage newPackage = manager.gameObject.AddComponent<WaitAIPackage>();
		newPackage.AttachManager(manager);
		newPackage.AttachAgent(agent);
		newPackage.waitCycles = new WaitCycle[Random.Range(1, 4)];
		float cycleDuration = Random.Range(1.5f, 3f);

		//Debug.Log("Creating wait package with " + newPackage.waitCycles.Length + " cycles of duration " + cycleDuration + ".");

		for (int j = 0; j < newPackage.waitCycles.Length; j++)
		{
			newPackage.waitCycles[j].turnAfterCycle = true;
			newPackage.waitCycles[j].waitTime = cycleDuration;
		}

		return newPackage;
	}

	public override void LogInfo()
	{
		Debug.Log("LogInfo: This is a Search AI Package.\n" +
			"InitialTarget: " + initialTarget + "\n" +
			"MinNumPatrols: " + minNumPatrols + "\n" +
			"MaxNumPatrols: " + maxNumPatrols + "\n" +
			"MaxSearchDistance: " + maxSearchDistance);
	}
}
