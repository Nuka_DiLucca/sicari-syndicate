﻿using UnityEngine;
using System.Collections;

public class guardAnimation : MonoBehaviour
{
    public NavMeshAgent myAgent;
    public Animator myAnim;
    public Rigidbody myRigidbody;
    public EnemyManager myEnemyManager;
    public SpriteRenderer myRenderer;
    public float walkSpeed;
    public float runSpeed;
    public float threashold;
    private bool isStunned = false;

    // Use this for initialization
    void Start()
    {
        myAgent = gameObject.GetComponent<NavMeshAgent>();
        myAnim = gameObject.GetComponent<Animator>();
        myRigidbody = gameObject.GetComponent<Rigidbody>();
        myEnemyManager = gameObject.GetComponent<EnemyManager>();
        myRenderer = gameObject.GetComponent<SpriteRenderer>();

        if (myEnemyManager != null)
            isStunned = myEnemyManager.IsStunned;
    }

    // Update is called once per frame
    void Update()
    {
        if (myEnemyManager != null)
        {
            isStunned = myEnemyManager.IsStunned;

            if (Mathf.Abs(myAgent.velocity.x) > 0 && Mathf.Abs(myAgent.velocity.x) < runSpeed)
            {
                myAnim.SetBool("isWalking", true);
                myAnim.SetBool("isRunning", false);
                myAnim.SetBool("isStunned", false);
            }
            if (Mathf.Abs(myAgent.velocity.x) > walkSpeed + threashold)
            {
                myAnim.SetBool("isWalking", false);
                myAnim.SetBool("isRunning", true);
                myAnim.SetBool("isStunned", false);
            }
            if (myAgent.velocity.x == 0f && isStunned == false)
            {
                myAnim.SetBool("isWalking", false);
                myAnim.SetBool("isRunning", false);
                myAnim.SetBool("isStunned", false);
            }
            if (isStunned == true)
            {
                myAnim.SetBool("isWalking", false);
                myAnim.SetBool("isRunning", false);
                myAnim.SetBool("isStunned", true);
            }
        }
        else if (myRigidbody != null)
        {
            if (Mathf.Abs(myRigidbody.velocity.x) > 0 && Mathf.Abs(myRigidbody.velocity.x) < runSpeed)
            {
                myAnim.SetBool("isWalking", true);
                myAnim.SetBool("isRunning", false);
                myAnim.SetBool("isStunned", false);
            }
            if (Mathf.Abs(myRigidbody.velocity.x) > walkSpeed + threashold)
            {
                myAnim.SetBool("isWalking", false);
                myAnim.SetBool("isRunning", true);
                myAnim.SetBool("isStunned", false);
            }

            if (myRigidbody.velocity.x == 0f)
            {
                myAnim.SetBool("isWalking", false);
                myAnim.SetBool("isRunning", false);
                myAnim.SetBool("isStunned", false);
            }
            else if (myRenderer != null)
            {
                if (myRigidbody.velocity.x > 0f)
                    myRenderer.flipX = false;
                else
                    myRenderer.flipX = true;
            }
        }
    }
}
