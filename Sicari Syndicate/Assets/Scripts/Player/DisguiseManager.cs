﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DisguiseManager : MonoBehaviour
{
	public static DisguiseManager primaryManager;

	public float batteryUsagePerSecond = 20f;
	public int storedDisguiseMaxNum = 3;
	public bool oneUse = true;
	public Disguise currentDisguise;
	public Color disguiseColor = new Color(0.882f, 1f, 0.784f, 0.882f);

	private SpriteRenderer myRenderer;
	private Animator myAnim;
	private Disguise kaidaBase;
	private Color baseColor;
	//private Disguise[] storedDisguises;
	private List<StoredDisguise> storedDisguises = new List<StoredDisguise>();
	//private int nextStorageIndex = 0;
	private bool isStorageFull = false;
	private IEnumerator batteryCoroutine;

	public delegate void DisguiseEventHandler();
	public static event DisguiseEventHandler PlayerDisguiseChange;

	public static void RaisePlayerDisguiseChange()
	{
		if (PlayerDisguiseChange != null)
			PlayerDisguiseChange();
	}

	void Awake()
	{
		if (primaryManager == null)
			primaryManager = this;

		BatteryManager.OnBatteryEmpty += EndDisguiseOnEmpty;

		//kaidaBase = new Disguise(gameObject.GetComponent<SpriteRenderer>(), gameObject.GetComponent<Animator>(), "Player", "Kaida");

		myRenderer = gameObject.GetComponent<SpriteRenderer>();
		myAnim = gameObject.GetComponent<Animator>();

		if (myRenderer != null && myAnim != null)
		{
			kaidaBase = new Disguise(myRenderer.sprite, myAnim.runtimeAnimatorController, "Player", "Kaida");
			currentDisguise = kaidaBase;
			baseColor = myRenderer.color;
		}

		//storedDisguises = new Disguise[storedDisguiseMaxNum];
	}

    /// <summary>
    /// Checks whether Kaida is currently disguised.
    /// </summary>
    public bool GetIsDisguised()
    {
        return (currentDisguise != kaidaBase);
    }

	/// <summary>
	/// Sets the maximum number of disguises to store. If there are now too many stored disguises, old disguises are deleted.
	/// </summary>
	/// <param name="newSize">New maximum storage size.</param>
	public void SetStoredDisguiseMaximum(int newSize)
	{
		if (newSize >= 0)
		{
			while (storedDisguises.Count > newSize)
				RemoveOldestDisguise();

			storedDisguiseMaxNum = newSize;
		}
	}

	/// <summary>
	/// Adds the provided disguise into storage.
	/// </summary>
	/// <param name="newDisguise">New disguise to be stored.</param>
	public bool AddDisguise(Disguise newDisguise)
	{
		if (newDisguise != null && storedDisguiseMaxNum > 0)
		{
			bool isNew = true;

			foreach (StoredDisguise storedDisguise in storedDisguises)
			{
				if (newDisguise.copiedSprite == storedDisguise.disguise.copiedSprite)
				{
					bool hasSameTags = true;

					for (int i = 0; i < newDisguise.disguiseTags.Count; i++)
					{
						if (!DisguiseHasTag(storedDisguise.disguise, newDisguise.disguiseTags[i]))
						{
							hasSameTags = false;
							break;
						}
					}

					if (hasSameTags)
						isNew = false;
				}

				if (!isNew)
					break;
			}

			if (isNew)
			{
				if (storedDisguises.Count >= storedDisguiseMaxNum)
					isStorageFull = true;

				//if (nextStorageIndex >= storedDisguiseMaxNum || nextStorageIndex < 0)
				//	nextStorageIndex = 0;

				//if (nextStorageIndex < storedDisguiseMaxNum)

				if (isStorageFull)
				{
					//storedDisguises.RemoveAt(nextStorageIndex);
					//storedDisguises[nextStorageIndex] = newDisguise;
					//storedDisguises.Add(new StoredDisguise(newDisguise, Time.time));
					//nextStorageIndex++;

					RemoveOldestDisguise();
				}

				storedDisguises.Add(new StoredDisguise(newDisguise, Time.time));

				// Update the stored disguises on the HUD.
				if (HUDManager.primaryHUDManager != null)
					HUDManager.primaryHUDManager.UpdateDisguises(storedDisguises.ToArray());

				//Debug.Log("Disguise added.");

				return true;
			}
			//else
			//	Debug.Log("Disguise already in storage.");
		}
		//else
		//	Debug.Log("Disguise was not added.");

		return false;
	}

	/// <summary>
	/// Removes the oldest disguise currently in storage.
	/// </summary>
	public void RemoveOldestDisguise()
	{
		if (storedDisguises.Count > 0)
		{
			int oldestDisguiseIndex = 0;
			float oldestDisguiseAge = storedDisguises[0].age;

			for (int i = 0; i < storedDisguises.Count; i++)
			{
				StoredDisguise disg = storedDisguises[i];

				if (disg.age > oldestDisguiseAge)
				{
					oldestDisguiseIndex = i;
					oldestDisguiseAge = disg.age;
				}
			}

			RemoveDisguiseAt(oldestDisguiseIndex);

			// Update the stored disguises on the HUD.
			if (HUDManager.primaryHUDManager != null)
				HUDManager.primaryHUDManager.UpdateDisguises(storedDisguises.ToArray());
		}
	}

	/// <summary>
	/// Removes the disguise stored at the given index and moves all subsequent disguises up in position.
	/// </summary>
	/// <param name="index">Index of the disguise to remove.</param>
	public void RemoveDisguiseAt(int index)
	{
		if (storedDisguises.Count > index)
		{
			if (storedDisguises.Count > (index + 1))
			{
				storedDisguises[index] = storedDisguises[index + 1];
				index++;
				RemoveDisguiseAt(index);
			}
			else
			{
				storedDisguises.RemoveAt(index);
				isStorageFull = false;
			}

			if (HUDManager.primaryHUDManager != null)
				HUDManager.primaryHUDManager.UpdateDisguises(storedDisguises.ToArray());
		}
	}

	/// <summary>
	/// Swaps the current disguise to the one stored at the given index, provided that said index and the disguise stored there exist.
	/// </summary>
	/// <param name="index">Index of the stored disguise to swap to.</param>
	public bool SwapToDisguise(int index)
	{
		if (index >= 0 && storedDisguises.Count > index && currentDisguise != storedDisguises[index].disguise && myRenderer != null && myAnim != null)
		{
			if (BatteryManager.primaryBatteryManager != null && BatteryManager.primaryBatteryManager.currentBattery > 0f)
			{
                if (batteryUsagePerSecond < 0f)
					batteryUsagePerSecond = 0f;

				if (batteryCoroutine != null)
				{
					BatteryManager.primaryBatteryManager.EndBatteryUsage(batteryCoroutine);
					StopCoroutine(batteryCoroutine);
				}

				batteryCoroutine = BatteryManager.primaryBatteryManager.UseBattery(batteryUsagePerSecond);
				StartCoroutine(batteryCoroutine);

                myRenderer.sprite = storedDisguises[index].disguise.copiedSprite;
                myRenderer.color = disguiseColor;
                myAnim.runtimeAnimatorController = storedDisguises[index].disguise.copiedAnimatorCtrl;
                currentDisguise = storedDisguises[index].disguise;

				if (oneUse)
				{
					//Debug.Log("Remove disguise at " + index + ".");
					RemoveDisguiseAt(index);
				}

                RaisePlayerDisguiseChange();

                //Debug.Log("Disguise swapped.");

				return true;
            }
            else if (BatteryManager.primaryBatteryManager == null)
            {
                myRenderer.sprite = storedDisguises[index].disguise.copiedSprite;
                myRenderer.color = disguiseColor;
                myAnim.runtimeAnimatorController = storedDisguises[index].disguise.copiedAnimatorCtrl;
                currentDisguise = storedDisguises[index].disguise;

                RaisePlayerDisguiseChange();

                //Debug.Log("Disguise swapped.");

				return true;
            }
            //else
            //    Debug.Log("Disguise was not swapped.");
        }
		//else
		//	Debug.Log("Disguise was not swapped.");

		return false;
	}

	public void EndDisguiseOnEmpty()
	{
		Platformer3DUserControl controller = PlayerManager.currentController;

		if (currentDisguise != kaidaBase && controller != null)
			controller.PlayActionSound(controller.endDisguise);

		EndDisguise();
	}

	/// <summary>
	/// Resets the current disguise to the base disguise.
	/// </summary>
	public bool EndDisguise()
	{
		if (kaidaBase != null && currentDisguise != kaidaBase && myRenderer != null && myAnim != null)
		{
			myRenderer.sprite = kaidaBase.copiedSprite;
			myRenderer.color = baseColor;
			myAnim.runtimeAnimatorController = kaidaBase.copiedAnimatorCtrl;
			currentDisguise = kaidaBase;

			RaisePlayerDisguiseChange();

			if (BatteryManager.primaryBatteryManager != null && batteryCoroutine != null)
			{
				BatteryManager.primaryBatteryManager.EndBatteryUsage(batteryCoroutine);
				StopCoroutine(batteryCoroutine);
			}

			//Debug.Log("Disguise reset.");

			return true;
		}
		//else
		//	Debug.Log("Disguise was not reset.");

		return false;
	}

	/// <summary>
	/// Checks whether the current disguise has any of the input tags. Used by EnemyAlertZones to attempt player detection.
	/// </summary>
	/// <returns><c>true</c>, if the current disguise has one or more of the tags, <c>false</c> otherwise.</returns>
	/// <param name="inputTags">Tags that will be checked against.</param>
	public bool DisguiseHasTag(params string[] inputTags)
	{
		if (currentDisguise != null)
		{
			/*
			foreach (string tag in inputTags)
			{
				if (currentDisguise.disguiseTags.Contains(tag.ToLower()))
					return true;
			}

			return false;
			*/

			return DisguiseHasTag(currentDisguise, inputTags);
		}
		else
			return true;
	}

	/// <summary>
	/// Checks whether the given disguise has any of the input tags.
	/// </summary>
	/// <returns><c>true</c>, if the given disguise has one or more of the tags, <c>false</c> otherwise.</returns>
	/// <param name="disguise">The disguise against which tags will be checked.</param>
	/// <param name="inputTags">Tags that will be checked against.</param>
	public bool DisguiseHasTag(Disguise disguise, params string[] inputTags)
	{
		if (disguise != null)
		{
			foreach (string tag in inputTags)
			{
				if (disguise.disguiseTags.Contains(tag.ToLower()))
					return true;
			}

			return false;
		}
		else
			return true;
	}

	void OnDestroy()
	{
		BatteryManager.OnBatteryEmpty -= EndDisguiseOnEmpty;

		if (primaryManager == this)
			primaryManager = null;
	}
}

public class Disguise
{
	//public SpriteRenderer copiedRenderer;
	//public Animator copiedAnimator;
	public Sprite copiedSprite;
	public RuntimeAnimatorController copiedAnimatorCtrl;
	public List<string> disguiseTags = new List<string>();

	//public Disguise(SpriteRenderer rend, Animator anim)
	public Disguise(Sprite sprite, RuntimeAnimatorController animCtrl)
	{
		//if (rend != null && anim != null)
		if (sprite != null)
		{
			//copiedRenderer = rend;
			copiedSprite = sprite;
		}

		if (animCtrl != null)
		{
			//copiedAnimator = anim;
			copiedAnimatorCtrl = animCtrl;
		}
	}

	//public Disguise(SpriteRenderer rend, Animator anim, params string[] tags)
	public Disguise(Sprite sprite, RuntimeAnimatorController animCtrl, params string[] tags)
	{
		//if (rend != null && anim != null)
		if (sprite != null)
		{
			//copiedRenderer = rend;
			copiedSprite = sprite;
		}

		if (animCtrl != null)
		{
			//copiedAnim = anim;
			copiedAnimatorCtrl = animCtrl;
		}

		for (int i = 0; i < tags.Length; i++)
		{
			if (tags[i] != null && !tags[i].Equals(string.Empty) && !disguiseTags.Contains(tags[i].ToLower()))
				disguiseTags.Add(tags[i].ToLower());
		}
	}
}

public struct StoredDisguise
{
	public Disguise disguise;
	public float age;

	public StoredDisguise(Disguise inDisguise, float inAge)
	{
		disguise = inDisguise;
		age = inAge;
	}
}
