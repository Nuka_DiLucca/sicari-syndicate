﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

/// <summary>
/// Contains classes and controls for various menus.
/// </summary>
namespace MenuControl
{
    /// <summary>
    /// Controls an assigned menu. 
    /// </summary>
    [RequireComponent(typeof(Canvas))]
    [RequireComponent(typeof(Animator))]
    public class MenuController : MonoBehaviour
    {
        #region Variables
        /// <summary>
        /// The Event System that controls the menus.
        /// </summary>
        [SerializeField] private EventSystem _eventSys;
        
        /// <summary>
        /// The Animator that controls opening and closing animations.
        /// </summary>
        private Animator _anim;

        /// <summary>
        /// The amount of time (in seconds) it takes for the window to rescale to the next window.
        /// </summary>
        [SerializeField] private float _transitionTime = 1;

        /// <summary>
        /// Timer used when transitioning from one window to another.
        /// </summary>
        private float _tTimer = 0;

        /// <summary>
        /// The window frame's GameObject.
        /// </summary>
        [SerializeField] private GameObject _frame;

        /// <summary>
        /// The window currently accessed.
        /// </summary>
        private MenuWindow _currentWindow;

        /// <summary>
        /// All of the windows this MenuController controls. The window at index 0 is the first window accessed when the menu opens.
        /// </summary>
        [SerializeField] private MenuWindow[] _windows;
        #endregion

        #region Functions
        /// <summary>
        /// Closes the current window and opens a new one over a set time.
        /// </summary>
        /// <param name="window">The window to open.</param>
        /// <returns></returns>
        private IEnumerator OpenWindow(MenuWindow window)
        {
            // Closes the current window group.
            _eventSys.SetSelectedGameObject(null);
            if (_currentWindow != null && _currentWindow.windowGroup != null)
                _currentWindow.windowGroup.SetActive(false);

            // The rate at which the window transforms per second.
            float xPosRate = (window.position.x - _frame.transform.localPosition.x) / _transitionTime;
            float yPosRate = (window.position.y - _frame.transform.localPosition.y) / _transitionTime;
            float xScaleRate = (window.scale.x - _frame.transform.localScale.x) / _transitionTime;
            float yScaleRate = (window.scale.y - _frame.transform.localScale.y) / _transitionTime;

            // Scales the frame to the new size over the time specified by _transitionTime.
            float newXPos, newYPos, newXScale, newYScale;
            while (_tTimer < _transitionTime)
            {
                newXPos = _frame.transform.localPosition.x + (xPosRate * Time.unscaledDeltaTime);
                newYPos = _frame.transform.localPosition.y + (yPosRate * Time.unscaledDeltaTime);
                newXScale = _frame.transform.localScale.x + (xScaleRate * Time.unscaledDeltaTime);
                newYScale = _frame.transform.localScale.y + (yScaleRate * Time.unscaledDeltaTime);

                _frame.transform.localPosition = new Vector2(newXPos, newYPos);
                _frame.transform.localScale = new Vector2(newXScale, newYScale);

                _tTimer += Time.unscaledDeltaTime;
                yield return new WaitForEndOfFrame();
            }

            // Sets the frame to the correct size in-case any unwanted offsets occurred.
            _frame.transform.localPosition = window.position;
            _frame.transform.localScale = window.scale;

            // Opens the new window group.
            _currentWindow = window;
            _currentWindow.windowGroup.SetActive(true);
            _eventSys.SetSelectedGameObject(_currentWindow.firstButton.gameObject);
            _tTimer = 0;
        }
        /// <summary>
        /// Opens a window based on its index number.
        /// </summary>
        public int OpenWindowByIndex { set { StartCoroutine(OpenWindow(_windows[value])); } }
        /// <summary>
        /// Opens a window based on its name.
        /// </summary>
        public string OpenWindowByName
        {
            set
            {
                for (int i = 0; i < _windows.Length; i++)
                    if (_windows[i].name == value)
                    {
                        StartCoroutine(OpenWindow(_windows[i]));
                        return;
                    }
                Debug.LogError("No window with name \"" + name + "\"exists.");
            }
        }

        /// <summary>
        /// Closes the menu.
        /// </summary>
        public void CloseMenu() { _anim.SetTrigger("close"); }

        /// <summary>
        /// 
        /// </summary>
        public void DeactivateCurrentWindow()
        {
            if (_currentWindow != null)
                _currentWindow.windowGroup.SetActive(false);
        }

        /// <summary>
        /// 
        /// </summary>
        public void ChangeMenuState() { _anim.SetTrigger("change"); }
        #endregion

        #region Misc. Functions
        /// <summary>
        /// Loads a scene based on its index number.
        /// </summary>
        public int LoadSceneByIndex
        {
            set
            {
                Time.timeScale = 1;
                SceneManager.LoadScene(value);
            }
        }
        /// <summary>
        /// Loads a scene based on its name.
        /// </summary>
        public string LoadSceneByName
        {
            set
            {
                Time.timeScale = 1;
                SceneManager.LoadScene(value);
            }
        }

        /// <summary>
        /// Sets the timeScale variable.
        /// </summary>
        public float TimeScale
        {
            get { return Time.timeScale; }
            set { Time.timeScale = value; }
        }

        

        /// <summary>
        /// Closes the application.
        /// </summary>
        public void Quit() { Application.Quit(); }
        #endregion

        #region MonoBehaviors
        /// <summary>
        /// Assigns variables.
        /// </summary>
        private void Awake()
        {
            if (_eventSys == null)
                _eventSys = FindObjectOfType<EventSystem>();

            _anim = GetComponent<Animator>();
        }

        /// <summary>
        /// Initializes the state of this object and assigned variables.
        /// </summary>
        private void Start()
        {
            if (_windows.Length <= 0)
                Debug.LogError("No windows specified to be controlled by MenuController attached to " + gameObject.name);

            for (int i = 0; i < _windows.Length; i++)
                _windows[i].windowGroup.SetActive(false);

            _eventSys.SetSelectedGameObject(null);
        }
        #endregion
    }
}