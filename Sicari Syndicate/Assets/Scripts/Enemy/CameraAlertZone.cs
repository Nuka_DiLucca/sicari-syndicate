﻿using UnityEngine;
using System.Collections;

public class CameraAlertZone : MonoBehaviour
{
	public CameraManager manager;

	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "Player" && manager != null)
		{
			manager.SubscribeToPlayerEvents();

			DisguiseManager dm = col.gameObject.GetComponent<DisguiseManager>();

			// If the player object is not one with a disguise manager, alert is automatic.
			if (dm == null)
				manager.PlayerFound();
			else if (dm.DisguiseHasTag(manager.tagsThatAlert))
				manager.PlayerFound();
			else
				Debug.Log("These are not the Kaidas you're looking for.");
		}
		//TODO: Implement an alert that takes this downed enemy's location.
		else if (col.gameObject.tag == "Enemy" && manager != null)
		{
			EnemyManager enemyScript = col.GetComponent<EnemyManager>();

			if (enemyScript != null && enemyScript.IsStunned)
			{
				// Temporary function.
				manager.BodyFound();
			}
		}
	}

	void OnTriggerExit(Collider col)
	{
		if (col.gameObject.tag == "Player" && manager != null)
		{
			manager.UnsubscribeFromPlayerEvents();
		}
	}

	void OnDisable()
	{
		if (manager != null)
		{
			manager.UnsubscribeFromPlayerEvents();
		}
	}
}
