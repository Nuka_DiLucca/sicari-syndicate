using System;
using UnityEngine;

/// <summary>
/// Script Template: Unity Technologies
/// Editor: Michael DiLucca
/// As of 10/27/2016 This script will be used for collision detection for the player controller
/// Please refer to this script when dealing with any and all player to character interactions
/// on the level of controlls and behaviors.
/// PLEASE NOTE: PlatformerCharacter2D IS CURRENTLY OBSOLETE!!!
/// </summary>

public class PlatformerCharacter3DKaida : PlatformerCharacter3D {
	
    [SerializeField]
	private float m_WalkSpeed = 1.5f;                    // The fastest the player can travel in the x axis.
    public float WalkSpeed { get { return m_WalkSpeed; } }
    [SerializeField]
	private float m_SneakSpeed = 0.8f;                    // The fastest the player can travel in the x axis.
    public float SneakSpeed { get { return m_SneakSpeed; } }
    //[SerializeField]
    //private float m_JumpForce = 2500f;                  // Amount of force added when the player jumps.

    private bool isWalking;
    public bool IsWalking { get { return isWalking; } }
    private bool isSneaking;
    public bool IsSneaking { get { return isSneaking; } }

    private bool isMoving;
    private EmitSound myEmitter;

    protected override void Awake()
    {
        base.Awake();

        myEmitter = GetComponent<EmitSound>();
    }

    // calculate acceleration and determine collisions
    // determine if we are disguised or not
    protected override void FixedUpdate() {

		if (!DisguiseManager.primaryManager.GetIsDisguised()) {

            if (acceleration.x != 0 || Mathf.Abs(m_Rigidbody.velocity.x) >= 0.05f)
            {
                anim.SetBool("isMoving", true);
                if (!isMoving)
                {
                    isMoving = true;
                    EmitSound.RaiseKaidaIsMoving(true);
                }
            }
            else
            {
                anim.SetBool("isMoving", false);
                if (isMoving)
                {
                    isMoving = false;
                    EmitSound.RaiseKaidaIsMoving(false);
                }
            }
		}
		
		base.FixedUpdate ();
    }


	public override void Move(float horMove, float verMove, bool jump) {
		
        if (!DisguiseManager.primaryManager.GetIsDisguised()) {
			
            if (horMove > 0)
                anim.SetBool("isRight", true);
            if (horMove < 0)
                anim.SetBool("isRight", false);
        }

        //only control the player if grounded or airControl is turned on
        if (m_Grounded) {

			// If we press the Walk button
            if (Input.GetButtonDown("Walk")) {
                isWalking = !isWalking;
				isSneaking = false;

				if (isWalking)
					m_MaxSpeed = m_WalkSpeed;
				else
					m_MaxSpeed = m_RunSpeed;

                if (!DisguiseManager.primaryManager.GetIsDisguised())
                {
                    anim.SetBool("isWalking", isWalking);
                    anim.SetBool("isSneaking", isSneaking);
                }

                if (myEmitter != null)
                    myEmitter.ResizeSphere();
            }

			// If we press the Sneak button
            if (Input.GetButtonDown("Sneak")) {
				
                isSneaking = !isSneaking;
                isWalking = false;

				if (isSneaking)
					m_MaxSpeed = m_SneakSpeed;
				else
					m_MaxSpeed = m_RunSpeed;

                if (!DisguiseManager.primaryManager.GetIsDisguised())
                {
                    anim.SetBool("isSneaking", isSneaking);
                    anim.SetBool("isWalking", isWalking);
                }

                if (myEmitter != null)
                    myEmitter.ResizeSphere();
            }
				
            m_Rigidbody.velocity = new Vector2(horMove * m_MaxSpeed, m_Rigidbody.velocity.y);
 
        }

        /*// If the player should jump...
        if (m_Grounded && jump) {
            // Add a vertical force to the player.
            m_Grounded = false;
            m_Rigidbody.AddForce(new Vector2(0f, m_JumpForce));
        }*/
    }
}
