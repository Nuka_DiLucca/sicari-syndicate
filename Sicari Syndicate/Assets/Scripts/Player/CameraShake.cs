﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour {

	public static bool Shaking; 
	private static float ShakeDecay;
	private static float ShakeIntensity;    
	private static Vector2 OriginalPos;

	void Start()
	{
		OriginalPos = new Vector2();
		Shaking = false;   
	}
		
	void Update () 
	{
		if(ShakeIntensity > 0)
		{
			transform.position = OriginalPos + Random.insideUnitCircle * ShakeIntensity;
			ShakeIntensity -= ShakeDecay;
		}
		else if (Shaking)
		{
			Shaking = false;  
		}
	}
		
	public static void Shake() {
		ShakeIntensity = 0.3f;
		ShakeDecay = 0.02f;
		Shaking = true;
	}
}