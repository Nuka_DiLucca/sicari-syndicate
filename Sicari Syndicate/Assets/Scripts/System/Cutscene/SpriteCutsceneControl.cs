﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SpriteCutsceneControl : MonoBehaviour
{
    #region Variables
    private Animator _anim;
    [SerializeField] private Text _textBox;
    
    public float typeTime = 0.025f;
    public float skipSpeed = 10;
    public Animator[] characterAnims;
    [TextArea(0, 3)] public string[] messages;
    [SerializeField] public string _speaker;
    #endregion

    #region Getters & Setters
    private Animator Anim
    {
        get { return _anim; }
        set { _anim = value; }
    }

    private Text TextBox
    {
        get { return _textBox; }
        set { _textBox = value; }
    }

    public string Speaker
    {
        get { return _speaker; }
        set { _speaker = value; }
    }
    #endregion

    #region Functions
    public int SceneByID { set { SceneManager.LoadScene(value); } }
    public string SceneByString { set { SceneManager.LoadScene(value); } }

    public void SetBoolToTrue(string name) { Anim.SetBool(name, true); }
    public void SetBoolToFalse(string name) { Anim.SetBool(name, false); }
    public void SetChar0Anim(int id) { characterAnims[0].SetInteger("animationID", id); }
    public void SetChar1Anim(int id) { characterAnims[1].SetInteger("animationID", id); }
    public void SetChar2Anim(int id) { characterAnims[2].SetInteger("animationID", id); }
    public void SetChar3Anim(int id) { characterAnims[3].SetInteger("animationID", id); }

    public void ClearTextBox() { TextBox.text = ""; }
    public void StartTypingText(int messageID) { StartCoroutine(TypeText(messages[messageID])); }
    /// <summary>
    /// Types out the text, one character at a time.
    /// </summary>
    /// <param name="text">The message to type out.</param>
    /// <returns></returns>
    private IEnumerator TypeText(string text)
    {
        Anim.SetBool("textFinished", false);
        ClearTextBox();
        TextBox.text = Speaker + '\n';
        for (int i = 0; i < text.Length; i++)
        {
            TextBox.text += text[i];
            yield return new WaitForSeconds(typeTime);
        }
        Anim.SetBool("next", false);
        Anim.SetBool("textFinished", true);
    }
    #endregion

    private void Start()
    {
        Anim = GetComponent<Animator>();
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Anim.SetBool("skip", true);
            Anim.speed = skipSpeed;
        }
        else if (Input.anyKey)
            Anim.SetBool("next", true);
    }
}