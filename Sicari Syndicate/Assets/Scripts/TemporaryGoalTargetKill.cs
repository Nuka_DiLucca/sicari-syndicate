﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class TemporaryGoalTargetKill : MonoBehaviour
{
	public AudioClip killSound;
    public int nextScene;

	private bool canKill = false;
    private IEnumerator waitForKillRoutine;

    private void Awake()
    {
        
    }

    private void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "Player" && EnemyObserver.kaida != null)
		{
			canKill = true;

            waitForKillRoutine = WaitForKill();
            StartCoroutine(waitForKillRoutine);
		}
	}

    private void OnTriggerExit(Collider col)
	{
		if (col.gameObject.tag == "Player" && EnemyObserver.kaida != null)
		{
			canKill = false;
		}
	}

	private IEnumerator WaitForKill()
	{
        while (canKill)
        {
            if (Input.GetButtonDown("Jump"))
            {
                canKill = false;
                StartCoroutine(DoWin());
            }

            yield return new WaitForEndOfFrame();
        }
	}

	private IEnumerator DoWin()
	{
		Debug.Log("Won.");

		PlayerManager.currentController.PlayActionSound(killSound);

		if (EnemyObserver.kaida.win != null)
			EnemyObserver.kaida.win.enabled = true;
		else
			Debug.Log("No win image found in stored Platform3DUserControl.");

		EnemyObserver.kaida.enabled = false;

        yield return new WaitForSeconds(4f);

		Time.timeScale = 1f;

		foreach (EnemyManager enemy in EnemyObserver.enemies)
			enemy.Unsubscribe();
		EnemyObserver.enemies.Clear();

		SceneManager.LoadScene(nextScene);

		if (EnemyObserver.kaida.win != null)
			EnemyObserver.kaida.win.enabled = false;
		else
			Debug.Log("No win image found in stored Platform3DUserControl.");
	}
}
