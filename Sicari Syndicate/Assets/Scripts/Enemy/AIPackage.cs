﻿using UnityEngine;
using System.Collections;

public abstract class AIPackage : MonoBehaviour {

    protected EnemyManager manager;
    protected NavMeshAgent agent;

	/// <summary>
	/// Attaches the GameObject's EnemyManger to this patrol algorithm.
	/// 
	/// The EnemyManager will be notified when this patrol ends, and potentially of other events.
	/// </summary>
	public void AttachManager()
	{
		manager = GetComponent<EnemyManager>();
	}

    /// <summary>
    /// Attaches a specific EnemyManger to this patrol algorithm.
    /// 
    /// The EnemyManager will be notified when this patrol ends, and potentially of other events.
    /// </summary>
    /// <param name="input">The provided EnemyManager instance.</param>
	public void AttachManager(EnemyManager input)
    {
		if (input != null)
			manager = input;
		else
			AttachManager();
    }

	/// <summary>
	/// Attaches the GameObject's NavMeshAgent to this patrol algorithm.
	/// 
	/// The NavMeshAgent may or may not be manipulated, depending on the particular algorithm.
	/// </summary>
	public void AttachAgent()
	{
		agent = GetComponent<NavMeshAgent>();
	}

    /// <summary>
    /// Attaches a specific NavMeshAgent to this patrol algorithm.
    /// 
    /// The NavMeshAgent may or may not be manipulated, depending on the particular algorithm.
    /// </summary>
    /// <param name="input">The provided NavMeshAgent instance</param>
    public void AttachAgent(NavMeshAgent input)
    {
		if (input != null)
			agent = input;
		else
			AttachAgent();
    }

    public abstract void StartPackage();

    /// <summary>
    /// Sends a message to the attached EnemyManager that this patrol has ended.
    /// 
    /// This method can be overriden with additional end-of-patrol functionality in derived AIPackage classes,
    /// but this base functionality should be called at the end.
    /// </summary>
    protected virtual void EndPackage()
    {
        if (manager != null)
            manager.NotifyEndPatrol(this);
    }

	/// <summary>
	/// Prints the AIPackage's info to the console.
	/// </summary>
	public abstract void LogInfo();
}
