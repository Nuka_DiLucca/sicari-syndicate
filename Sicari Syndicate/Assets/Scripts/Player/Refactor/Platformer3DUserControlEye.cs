using System;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

/// <summary>
/// Script Template: Unity Technologies
/// Editor: Michael DiLucca
/// As of 10/27/2016 This script will be used for collision detection for the player controller
/// Please refer to this script when dealing with any and all player to character interactions
/// on the level of controlls and behaviors.
/// PLEASE NOTE: Platformer2DUserControl IS CURRENTLY OBSOLETE!!!
/// </summary>

[RequireComponent(typeof(PlatformerCharacter3D))]
public class Platformer3DUserControlEye : Platformer3DUserControl
{

	public override void Start() {

		if (DisguiseManager.primaryManager != null)
			disguiseManager = DisguiseManager.primaryManager;
		
		base.Start ();
    }

	protected override void Update() {
	
		// Read the jump input in Update so button presses aren't missed.
        //if (!m_Jump) {
           
            //m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
        //}

		base.Update ();

        // VERY temporary stun function until this all gets reimplemented on proper character controllers.
        if (Input.GetButtonDown("Jump")) {
			
            Collider[] hits = Physics.OverlapSphere(transform.position, 0.3f);

            for (int i = 0; i < hits.Length; i++) {
				
                EnemyManager enemyScript = hits[i].gameObject.GetComponent<EnemyManager>();

				if (enemyScript != null) {
					if (enemyScript.IsStunned && disguiseManager != null) {
						
						SpriteRenderer newRend = enemyScript.gameObject.GetComponent<SpriteRenderer> ();
						Animator newAnim = enemyScript.gameObject.GetComponent<Animator> ();

						if (newRend != null && newAnim != null) {
							//Disguise newDisguise = new Disguise(newRend.sprite, newAnim.runtimeAnimatorController, enemyScript.tagsInDisguise);
							Disguise newDisguise = new Disguise (enemyScript.disguiseSprite, newAnim.runtimeAnimatorController, enemyScript.tagsInDisguise);
							if (disguiseManager.AddDisguise (newDisguise))
								PlayActionSound (scanSound);
						}
					}
				} 

				HackableObject hackObj = hits [i].gameObject.GetComponent<HackableObject> ();

				if (hackObj != null) {
					hackObj.Hack ();
				}
            }
        }
    }
}

