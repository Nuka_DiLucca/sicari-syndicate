﻿using System;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Contains classes and controls for various menus and windows.
/// </summary>
namespace MenuControl
{
    /// <summary>
    /// Contains attribute of a menu window. 
    /// </summary>
    [Serializable]
    public class MenuWindow
    {
        #region Variables
        /// <summary>
        /// The name of the window.
        /// </summary>
        public string name;
        
        /// <summary>
        /// The group containing the window's Buttons and Objects.
        /// </summary>
        public GameObject windowGroup;
        
        /// <summary>
        /// The Button to be selected when the window opens.
        /// </summary>
        public Button firstButton;
        
        /// <summary>
        /// The position of the window on the Canvas.
        /// </summary>
        public Vector2 position;
        
        /// <summary>
        /// The size of the window.
        /// </summary>
        public Vector2 scale;
        #endregion

        #region Constructors
        /// <summary>
        /// Copy Constructor
        /// </summary>
        /// <param name="window">The window to copy.</param>
        public MenuWindow(MenuWindow window)
        {
            name = window.name;
            windowGroup = window.windowGroup;
            firstButton = window.firstButton;
            position = window.position;
            scale = window.scale;
        }
        
        /// <summary>
        /// Vector Constructor
        /// </summary>
        /// <param name="name">The name of the window.</param>
        /// <param name="windowGroup">The group that contains the window's buttons and other objects.</param>
        /// <param name="firstButton">The button to be selected when the window opens.</param>
        /// <param name="position">The position of the window on the Canvas.</param>
        /// <param name="scale">The size of the window.</param>
        public MenuWindow(string name, GameObject windowGroup, Button firstButton, Vector2 position, Vector2 scale)
        {
            this.name = name;
            this.windowGroup = windowGroup;
            this.firstButton = firstButton;
            this.position = position;
            this.scale = scale;
        }
        
        /// <summary>
        /// Float Constructor
        /// </summary>
        /// <param name="name"The name of the window.></param>
        /// <param name="windowGroup">The group that contains the window's buttons and other objects.</param>
        /// <param name="firstButton">The button to be selected when the window opens.</param>
        /// <param name="xPosition">The position of the window on the Canvas's x-axis.</param>
        /// <param name="yPosition">The position of the window on the Canvas's y-axis.</param>
        /// <param name="xScale">The width of the window.</param>
        /// <param name="yScale">The height of the window.</param>
        public MenuWindow(string name, GameObject windowGroup, Button firstButton, float xPosition, float yPosition, float xScale, float yScale)
        {
            this.name = name;
            this.windowGroup = windowGroup;
            this.firstButton = firstButton;
            position = new Vector2(xPosition, yPosition);
            scale = new Vector2(xScale, yScale);
        }
        #endregion
    }
}