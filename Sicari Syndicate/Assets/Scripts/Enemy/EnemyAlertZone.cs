﻿using UnityEngine;
using System.Collections;

public class EnemyAlertZone : MonoBehaviour
{
    public EnemyManager manager;

	void OnTriggerEnter(Collider col)
    {
		if (col.gameObject.tag == "Player" && manager != null)
		{
			manager.SubscribeToPlayerEvents();
			manager.WipeFollow();

			DisguiseManager dm = col.gameObject.GetComponent<DisguiseManager>();

			// If the player object is not one with a disguise manager, alert is automatic.
			if (dm == null)
				manager.PlayerFound();
			else if (dm.DisguiseHasTag(manager.tagsThatAlert))
				manager.PlayerFound();
			else
				Debug.Log("These are not the Kaidas you're looking for.");
		}
		else if (col.gameObject.tag == "Enemy" && manager != null)
		{
			EnemyManager enemyScript = col.GetComponent<EnemyManager>();

			if (enemyScript != null && enemyScript.IsStunned)
			{
				ReviveAllyAIPackage newPackage = manager.gameObject.AddComponent<ReviveAllyAIPackage>();
				newPackage.target = enemyScript;

				manager.ForceNewPackage(newPackage);
			}
		}
    }

	void OnTriggerExit(Collider col)
	{
		if (col.gameObject.tag == "Player" && manager != null)
		{
			manager.UnsubscribeFromPlayerEvents();
		}
	}

	void OnDisable()
	{
		if (manager != null)
		{
			manager.UnsubscribeFromPlayerEvents();
			manager.LazyUnsubscribeFromPlayerEvents();
		}
	}
}
