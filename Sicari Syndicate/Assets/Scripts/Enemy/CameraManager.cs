﻿using UnityEngine;
using System.Collections;

public class CameraManager : HackableObject
{
	public enum Direction
	{
		None,
		Clockwise,
		Counterclockwise
	}

	public float startZAngle = 0f;
	public GameObject myElec;
	public float targetZAngle = 180f;
	public Direction turningDirection = Direction.Clockwise;
	public GameObject alertZone;
	public string[] tagsThatAlert;

	private float _interruptedZAngle = 0f;
	private IEnumerator turningRoutine;

	void Awake()
	{
		myElec.SetActive (false);
		if (alertZone == null)
		{
			Transform child = transform.GetChild(0);

			if (child != null && child.GetComponent<CameraAlertZone>() != null)
			{
				alertZone = child.gameObject;
			}
		}
	}

	void Start()
	{
		if (turningDirection != Direction.None)
		{
			turningRoutine = CameraTurnRoutine(startZAngle, targetZAngle, turningDirection);
			StartCoroutine(turningRoutine);
		}
	}

	public IEnumerator CameraTurnRoutine(float start, float end, Direction direction)
	{
		transform.eulerAngles = new Vector3(0f, 0f, start);
		Vector3 currentAngle = transform.eulerAngles;
		Vector3 targetAngle = new Vector3(0f, 0f, end);

		//Debug.Log("Turning until " + end + " or " + (end - 360f) + ".");

		while (Mathf.Round(transform.eulerAngles.z) != end && Mathf.Round(transform.eulerAngles.z) != (end - 360f))
		{
			yield return new WaitForEndOfFrame();

			currentAngle = new Vector3(
				Mathf.LerpAngle(currentAngle.x, targetAngle.x, Time.deltaTime),
				Mathf.LerpAngle(currentAngle.y, targetAngle.y, Time.deltaTime),
				Mathf.LerpAngle(currentAngle.z, targetAngle.z, Time.deltaTime));

			transform.eulerAngles = currentAngle;
			//Debug.Log(Mathf.Round(transform.eulerAngles.z));
		}

		//Debug.Log("Camera reversing.");
		Reverse();
	}

	private void Reverse()
	{
		float temp = startZAngle;
		startZAngle = targetZAngle;
		targetZAngle = temp;

		if (turningDirection == Direction.Clockwise)
		{
			turningDirection = Direction.Counterclockwise;
		}
		else if (turningDirection == Direction.Counterclockwise)
		{
			turningDirection = Direction.Clockwise;
		}

		StopCoroutine(turningRoutine);
		turningRoutine = CameraTurnRoutine(startZAngle, targetZAngle, turningDirection);
		StartCoroutine(turningRoutine);
	}

    void OnTriggerStay(Collider other) {
        if (other.gameObject.tag == "Eye") {
			myElec.SetActive (true);
            Halt();
            gameObject.transform.GetChild(0).gameObject.SetActive(false);
        }
    }

    void OnTriggerExit(Collider other) {
        if (other.gameObject.tag == "Eye") {
			myElec.SetActive (false);
            Resume();
            gameObject.transform.GetChild(0).gameObject.SetActive(true);
        }
    }

    public override void Hack() {}

    public void Halt()
	{
		StopCoroutine(turningRoutine);
		_interruptedZAngle = transform.rotation.z;
	}

	public void Resume()
	{
		turningRoutine = CameraTurnRoutine(_interruptedZAngle, targetZAngle, turningDirection);
		StartCoroutine(turningRoutine);
	}

	public void Reset()
	{
		Halt();

		if (alertZone != null)
		{
			alertZone.SetActive(false);
			alertZone.SetActive(true);
		}

		Resume();
	}

	/// <summary>
	/// Subscribes this enemy to player events that affect its patrolling.
	/// </summary>
	public void SubscribeToPlayerEvents()
	{
		DisguiseManager.PlayerDisguiseChange += Reset;
	}

	/// <summary>
	/// Unsubscribes this enemy from player events that affect its patrolling.
	/// </summary>
	public void UnsubscribeFromPlayerEvents()
	{
		DisguiseManager.PlayerDisguiseChange -= Reset;
	}

	//TODO: Pass in a generated empty game object representing Kaida's last known location.
	/// <summary>
	/// Have an un-hacked camera send out an alert that the player has been found.
	/// </summary>
	public void PlayerFound()
	{
		Debug.Log("Camera found player.");

		if (!isHacked)
		{
			EnemyObserver.RaiseAlertAllOtherEnemies(null);
		}
	}

	//TODO: Pass in a generated empty game object representing the body's location.
	/// <summary>
	/// Have an un-hacked camera send out an alert that a body has been found.
	/// </summary>
	public void BodyFound()
	{
		Debug.Log("Camera found body.");

		if (!isHacked)
		{
			EnemyObserver.RaiseAlertAllOtherEnemies(null);
		}
	}
}
