﻿using UnityEngine;
using System.Collections;
using System;

public class WaitAIPackage : AIPackage
{
    public WaitCycle[] waitCycles;

    public override void StartPackage()
    {
        // Making sure the agent won't keep moving once we've begun waiting.
        if (agent != null)
            agent.destination = agent.transform.position;

        StartCoroutine(WaitForCycles(waitCycles));
    }

    private IEnumerator WaitForCycles(WaitCycle[] cycles)
    {
        foreach (WaitCycle cycle in cycles)
        {
            if (cycle.waitTime > 0f)
                yield return new WaitForSeconds(cycle.waitTime);

            // Temporary flip function until we implement proper animations.
            if (cycle.turnAfterCycle && manager != null)
                manager.Turn(!manager.IsFacingLeft);
        }

        EndPackage();
    }

	public override void LogInfo()
	{
		string logString = "LogInfo: This is a Wait AI Package.\n" +
			"NumCycles: " + waitCycles.Length;

		for (int i = 0; i < waitCycles.Length; i++)
		{
			logString += "\n----- Cycle " + i + " -----\n" +
				"WaitTime: " + waitCycles[i].waitTime + "\n" +
				"TurnAfterCycle: " + waitCycles[i].turnAfterCycle;
		}

		Debug.Log(logString);
	}
}

[System.Serializable]
public struct WaitCycle
{
    public float waitTime;
    public bool turnAfterCycle;
}
