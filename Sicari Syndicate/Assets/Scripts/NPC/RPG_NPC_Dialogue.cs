﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RPG_NPC_Dialogue : MonoBehaviour {

    public Text dialogueText;
    public KeyCode myKey;                               //What key we press 
    private bool chatLock = false;
    public string gateName = "junky";
    public GameObject gate;

    // Use this for initialization
    void Start() {
        dialogueText.transform.parent.gameObject.SetActive(false);
    }

    void OnTriggerStay(Collider other) {
        if (Input.GetKeyDown(myKey) && other.tag == "Player" && !chatLock) {
            dialogueText.text = FlagManager.getDialogue(transform.name);
            dialogueText.transform.parent.gameObject.SetActive(true);
            chatLock = true;
            if (FlagManager.checkGate(gateName))
                gate.SetActive(true);
            StartCoroutine(WaitAndPrint(1));
        }
    }

    void OnTriggerExit(Collider other) {
        dialogueText.transform.parent.gameObject.SetActive(false);
    }

    IEnumerator WaitAndPrint(float waitTime) {
        yield return new WaitForSeconds(waitTime);
        chatLock = false;
        print("WaitAndPrint " + Time.time);
    }
}
