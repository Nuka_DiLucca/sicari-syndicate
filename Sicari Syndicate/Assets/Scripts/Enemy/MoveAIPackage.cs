﻿using UnityEngine;
using System.Collections;
using System;

public class MoveAIPackage : AIPackage
{
    public Transform target;
    public float moveSpeed;
	public float threshold;

    public override void StartPackage()
    {
        if (agent != null)
        {
            if (target != null)
            {
                // Temporary flip function until we implement proper animations.
                if (manager != null)
                {
                    float diff = target.position.x - agent.transform.position.x;

                    if (diff > 0)
                        manager.Turn(false);
                    else if (diff < 0)
                        manager.Turn(true);
                }

                agent.destination = target.position;

                if (moveSpeed > 0)
                    agent.speed = moveSpeed;
                else
                    Debug.Log("Move speed was not set properly in MoveAIPackage. Defaulting to current agent speed.");

                this.enabled = true;
            }
            else
            {
                Debug.Log("Target was not set in MoveAIPackage. Ending patrol.");
                EndPackage();
                this.enabled = false;
            }

        }
        else
        {
            Debug.Log("Agent was not attached properly to MoveAIPackage. Ending patrol.");
            EndPackage();
            this.enabled = false;
        }
    }
	
	void Update()
    {
		if (agent != null && target != null && manager != null)
		{
			// For the case that we were given a moving target.
			agent.destination = new Vector3(target.position.x, target.position.y, manager.HardZ);

			// Temporary flip function until we implement proper animations.
			//if (manager != null)
			//{
			float diff = target.position.x - agent.transform.position.x;

			if (diff > 0)
				manager.Turn(false);
			else if (diff < 0)	
				manager.Turn(true);
			//}

			// This patrol will end when the agent has reached its destination, checked for here.
			if (!agent.pathPending)
			{
				if (agent.remainingDistance <= threshold || agent.remainingDistance <= agent.stoppingDistance)
				{
					EndPackage ();
					//Debug.Log("Done moving.");
					this.enabled = false;
				}
			}
		}
    }

	public override void LogInfo()
	{
		Debug.Log("LogInfo: This is a Move AI Package.\n" +
			"Target: " + target + "\n" +
			"MoveSpeed: " + moveSpeed + "\n" +
			"Threshold: " + threshold);
	}
}
