﻿using UnityEngine;
using System.Collections;

public class FlagManager : MonoBehaviour {

    public static bool clerkIntro = true;
    public static string clerkIntroString = "Hey, <color=orange>Cheetos</color>? That all? Okay, $16";
    public static string clerkString = "You're the lady who came in and bought <color=orange>Cheetos</color> earlier. Finished already?";

    public static bool junkyIntro = true;
    public static bool junkyReveal = false;
    public static string junkyIntroString = "Yo! Not gonna lie, but we need some <color=orange>Cheetos</color> to complete this phaze. If you find any, let us hive on em'.";
    public static string junkyWaitString = "Man we could really use some <color=orange>Cheetos</color> to pair with this ideal phaze we got goin'.";
    public static string junkyIntroRevealString = "<color=orange>Cheeeeeetos</color>! Oi, nice mods lady. If you looking to skip the line, here is the alley access code.";
    public static string junkyRevealString = "Just down the alley lady if you lookin' to touch up those mods.";

    public static string maxString = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer facilisis, urna nec fringilla pharetra, enim mi ornare niselethc";

    //	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer facilisis, urna nec fringilla pharetra, enim mi ornare niselethc
    //  Gah! What was it!.. It's on the <color=orange>tip of my tounge</color>. Hmm. 

    public static string getDialogue(string name) {
        switch (name) {

            case "Clerk":
                if (clerkIntro) {
                    clerkIntro = false;
                    return (clerkIntroString);
                }
                return (clerkString);


            case "Junkies":
                if (junkyReveal) {
                    return (junkyRevealString);
                }
                if (!clerkIntro) {
                    junkyIntro = false;
                    junkyReveal = true;
                    return (junkyIntroRevealString);
                }
                if (junkyIntro) {
                    junkyIntro = false;
                    return (junkyIntroString);
                }
                return (junkyWaitString);

            default:
                return ("Dialogue has no records of a Mr./Mrs. " + name + ".");
        }
    }

    public static bool checkGate(string name) {
        switch (name) {

            case "junky":
                return junkyReveal;

            default:
                return (false);
        }
    }
}
