﻿using UnityEngine;
using UnityEngine.UI;

public class HUDManager : MonoBehaviour
{
	public static HUDManager primaryHUDManager;
    [SerializeField] private Animator _menuAnim;

    public void ChangePauseState() { _menuAnim.SetTrigger("change"); }

    /*private bool _paused = false;
    public bool Paused
    {
        get { return _paused; }
        set
        {
            _paused = value;
            if (_paused)
                _menuAnim.SetTrigger("open");
            else
                _menuAnim.SetTrigger("close");
        }
    }*/

	public Image[] storedDisguiseFields;
	public Image winImage;
	public Image lossImage;

    public void UpdateDisguises(StoredDisguise[] storedDisguises)
	{
		Image current_field;
		StoredDisguise current_disguise;

		for (int i = 0; i < storedDisguiseFields.Length; i++)
		{
			current_field = storedDisguiseFields[i];

			if (current_field != null)
			{
				// If we haven't been passed enough disguises with which to populate the fields, extra fields get emptied.
				if (i >= storedDisguises.Length)
				{
					current_field.sprite = null;
					current_field.enabled = false;
				}
				else
				{
					current_disguise = storedDisguises[i];

					if (current_disguise.disguise != null)
					{
						current_field.sprite = current_disguise.disguise.copiedSprite;
						current_field.enabled = true;
					}
					else
					{
						current_field.sprite = null;
						current_field.enabled = false;
					}
				}
			}
		}
	}

	void OnDestroy()
	{
		if (primaryHUDManager == this)
			primaryHUDManager = null;
	}

    #region MonoBehaviors
    /// <summary>
    /// Assigns components.
    /// </summary>
    void Awake()
    {
        if (primaryHUDManager == null)
            primaryHUDManager = this;
    }

    /// <summary>
    /// Controls pausing when the player presses the pause button.
    /// </summary>
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            ChangePauseState();
            //Paused = !Paused;
    }
    #endregion
}