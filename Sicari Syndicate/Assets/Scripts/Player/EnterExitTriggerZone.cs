﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnterExitTriggerZone : MonoBehaviour {

	public bool isForegroundZone = true;
    public bool isVent = false;
	public EnterExitTriggerZone partnerZone;
    public GameObject indicator;
	public List<GameObject> objectsToEnableOrDisable = new List<GameObject>();

	void Awake()
	{
		if (partnerZone != null)
		{
			// In case we forgot to set the partner's field to this.
			if (partnerZone.partnerZone == null)
			{
				partnerZone.partnerZone.partnerZone = this;
			}
		}
	}

	void OnTriggerEnter(Collider other)
	{
		Platformer3DUserControl playerControl = other.GetComponent<Platformer3DUserControl>();

		if (playerControl != null)
		{
			if (indicator != null)
				indicator.SetActive(true);

            if (isVent)
            {
                if (other.gameObject.layer == 8)
                {
                    playerControl.SetCanEnter(true);
                    playerControl.SetCanExit(false);

                    playerControl.SetDestination(partnerZone.transform);
                }
                else
                {
                    playerControl.SetCanEnter(false);
                }
            }
			else if (isForegroundZone)
			{
				playerControl.SetCanEnter(true);
				playerControl.SetCanExit(false);

				playerControl.SetDestination(partnerZone.transform);
			}
			else
			{
				playerControl.SetCanEnter(false);
				playerControl.SetCanExit(true);

				playerControl.SetDestination(partnerZone.transform);
			}
		}
		else
		{
			EnemyManager enemyScript = other.GetComponent<EnemyManager>();

			if (enemyScript != null)
			{
				enemyScript.FollowNext();
			}
		}
	}

	void OnTriggerExit(Collider other)
	{
		Platformer3DUserControl playerControl = other.GetComponent<Platformer3DUserControl>();

		if (playerControl != null)
		{
            if (indicator != null)
                indicator.SetActive(false);

            playerControl.SetCanEnter(false);
			playerControl.SetCanExit(false);

			playerControl.SetDestination(null);
		}
	}

	public void EnableObjects()
	{
		foreach (GameObject obj in objectsToEnableOrDisable)
		{
			if (obj != null)
			{
				obj.SetActive(true);
			}
		}
	}

	public void DisableObjects()
	{
		foreach (GameObject obj in objectsToEnableOrDisable)
		{
			if (obj != null)
			{
				obj.SetActive(false);
			}
		}
	}
}
