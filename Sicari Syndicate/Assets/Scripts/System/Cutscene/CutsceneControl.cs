﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Contains classes and parameters used in cutscenes.
/// </summary>
namespace CutsceneManagement
{
    /// <summary>
    /// Provides functionality for cutscenes.
    /// </summary>
    [RequireComponent(typeof(Animator))]
    public class CutsceneControl : MonoBehaviour
    {
        #region Variables
        /// <summary>
        /// The cutscene's Animator.
        /// </summary>
        private Animator _anim;

        /// <summary>
        /// The AudioSource connected to the Animator.
        /// </summary>
        private AudioSource _soundSource;

        /// <summary>
        /// The Canvas that contains the loading animation.
        /// </summary>
        [SerializeField] private Canvas _loadingCanvas;

        /// <summary>
        /// The speed at which the cutscene fast forwards when the player presses the skip button.
        /// </summary>
        [SerializeField]
        private float _skipSpeed = 10;
        /// <summary>
        /// Returns the speed at which the cutscene fast forwards when the player presses the skip button.
        /// </summary>
        public float SkipSpeed { get { return _skipSpeed; } }

        /// <summary>
        /// Contains the various sound effects used in the cutscene.
        /// </summary>
        [SerializeField] private AudioClip[] _sounds;
        #endregion

        #region Functions
        /// <summary>
        /// Sets a given bool in the Animator to true.
        /// </summary>
        /// <param name="name">The name of the bool.</param>
        public void SetBoolToTrue(string name) { _anim.SetBool(name, true); }
        /// <summary>
        /// Sets a given bool in the Animator to false.
        /// </summary>
        /// <param name="name">The name of the bool.</param>
        public void SetBoolToFalse(string name) { _anim.SetBool(name, false); }

        /// <summary>
        /// Loads a scene based on its index number.
        /// </summary>
        /// <param name="index">The number of the scene in the Scene Manager.</param>
        public void LoadSceneByIndex(int index) { SceneManager.LoadScene(index); }
        /// <summary>
        /// Loads a scene based on its name.
        /// </summary>
        /// <param name="name">The name of the scene.</param>
        public void LoadSceneByName(string name) { SceneManager.LoadScene(name); }


        public int LoadingAnimationEnabled
        {
            get
            {
                if (_loadingCanvas != null && _loadingCanvas.enabled)
                    return 1;
                else
                    return 0;
            }
            set
            {
                if (_loadingCanvas != null)
                {
                    if (value > 0)
                        _loadingCanvas.gameObject.SetActive(true);
                    else
                        _loadingCanvas.gameObject.SetActive(false);
                }
            }
        }

        /// <summary>
        /// Plays the AudioClip from the _sounds array at the specified index.
        /// </summary>
        /// <param name="index">The index number of the AudioClip.</param>
        public void PlayOneShotByIndex(int index) { _soundSource.PlayOneShot(_sounds[index]); }
        /// <summary>
        /// Plays an AudioClip from the _sounds array that has the specified name.
        /// </summary>
        /// <param name="name">The name of the AudioClip.</param>
        public void PlayOneShotByName(string name)
        {
            for (int i = 0; i < _sounds.Length; i++)
                if (name == _sounds[i].name)
                {
                    _soundSource.PlayOneShot(_sounds[i]);
                    return;
                }
            Debug.LogError("No AudioClip of name " + name + " exists in the array.");
        }
        #endregion

        #region MonoBehaviors
        /// <summary>
        /// Assigns variables.
        /// </summary>
        private void Awake()
        {
            _anim = GetComponent<Animator>();
            _soundSource = GetComponent<AudioSource>();
            if (_loadingCanvas == null)
                _loadingCanvas = GameObject.FindGameObjectWithTag("LoadingCanvas").GetComponent<Canvas>();
        }

        /// <summary>
        /// Sets the starting states of components and variables.
        /// </summary>
        private void Start()
        {
            if (_loadingCanvas != null)
                _loadingCanvas.gameObject.SetActive(false);
        }

        /// <summary>
        /// Tracks the player's inputs and interacts with the cutscene's Animator accordingly.
        /// </summary>
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                _anim.SetBool("skip", true);
                _anim.speed = _skipSpeed;
            }
            if (Input.anyKeyDown)
                _anim.SetBool("next", true);
        }
        #endregion
    }
}