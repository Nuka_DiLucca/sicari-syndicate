﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyManager : MonoBehaviour
{
	/// The set of traits that defines special behavior for this character.
	public EnemyPersonalityTraits behaviours = new EnemyPersonalityTraits();

	/// The animation clip for waking up from a stun.
    public AnimationClip wakeUpSelfClip;
	/// The animation clip for waking up another guard from a stun.
    public AnimationClip wakeUpOtherClip;
	/// Default looping sequence of patrol behaviors.
	public AIPackage[] normalPatrols;
	/// Sequence of patrol behaviors when first going on alert (i.e. after first finding the player).
	public AIPackage[] initialAlertPatrols;
	/// Looping sequence of patrol behaviors when on alert.
	public AIPackage[] alertedPatrols;
	/// Looping, temporary sequence of patrol behaviors that deal with finding Kaida when given a "last seen" location.
	public AIPackage[] searchPatrols;
	/// Length of time for which this enemy's stunned state will last.
	public float stunnedTime = 5f;
	/// Length of time for which this enemy will search for the player before giving up.
	public float timeToSearch = 3f;
	/// Length of time for which this enemy will track player movements after they leave its vision.
	public float playerTrackBuffer = 1f;
	/// TEMPORARY. Renderer containing an icon that represents an "alerted" state for this enemy.
    public SpriteRenderer alertImage;
	/// TEMPORARY. Renderer containing an icon that represents a "stunned" state for this enemy.
	public SpriteRenderer stunImage;
	/// The "vision cone" active when this enemy is facing left.
    public GameObject leftVisionField;
	/// The "vision cone" active when this enemy is facing right.
	public GameObject rightVisionField;
	/// The sprite for the disguise created from scanning this enemy.
    public Sprite disguiseSprite;
	/// The tags on the disguise created from scanning this enemy.
	public string[] tagsInDisguise;
	/// The tags that will alert this enemy to Kaida's presence when they see her. Defaults to "Kaida" and "Player".
	public string[] tagsThatAlert;
    [HideInInspector]
    public Animator anim;

	/// The currently enacted sequence of patrols. May or may not loop, depending on which specific sequence it is.
    private AIPackage[] _currentSequence;
	public AIPackage[] CurrentSequence { get { return _currentSequence; } }
	/// The index of the currently active patrol in the current sequence.
	private int _currentIndex = 0;
	/// The most recent sequence of patrols that was forcibly halted to make way for some other sequence.
	private AIPackage[] _interruptedSequence;
	/// The index of the formerly active patrol in the most recent interrupted sequence.
	private int _interruptedIndex = 0;
	/// A temporary package outside of the enemy's sequences.
	private AIPackage tempPackage;
	/// The coroutine for the enemy's timed search after being alerted.
	private IEnumerator searchRoutine;
    private NavMeshAgent _agent;
    private SpriteRenderer _renderer;
	/// The place to which this enemy will first attempt to follow Kaida, after seeing her move through an entrance or exit.
	private Transform followToLoc;
	/// The backlog of further places to which this enemy will attempt to follow Kaida, after seeing her move through entrances or exits.
	private List<Transform> followLocBacklog;

	/// The Z value that this character will be locked to. Can change on entrance/exit.
	private float hardZ;
	/// The Z value that this character will be locked to. Can change on entrance/exit.
	public float HardZ
	{
		get
		{
			return hardZ;
		}
	}

    private bool _isFacingLeft;
    public bool IsFacingLeft
    {
        get
        {
            return _isFacingLeft;
        }
    }

    private bool _isStunned = false;
    public bool IsStunned
    {
        get
        {
            return _isStunned;
        }
    }

	// Not currently implemented, but will play into a fight or flight patrol sequence.
	private bool _isAlerted = false;

	void Awake()
    {
		EnemyObserver.enemies.Add(this);
		//foreach (EnemyManager man in EnemyObserver.enemies)
		//	Debug.Log(gameObject.name + ": " + man.gameObject.name);

		EnemyObserver.AlertAllOtherEnemies += ReceiveAlert;

		hardZ = transform.position.z;

        _renderer = gameObject.GetComponent<SpriteRenderer>();
        _agent = gameObject.GetComponent<NavMeshAgent>();
        anim = gameObject.GetComponent<Animator>();

		followLocBacklog = new List<Transform>();

        if (_agent != null)
            _agent.updateRotation = false;

		if (tagsThatAlert == null || tagsThatAlert.Length == 0)
		{
			tagsThatAlert = new string[2];
			tagsThatAlert[0] = "Kaida";
			tagsThatAlert[1] = "Player";
		}

        SubscribePatrols();

		// We attempt to enter patrol 0 of the "patrols" patrol sequence first of anything.
		if (normalPatrols.Length > 0)
		{
			EnterPatrolSequence(normalPatrols, 0);
		}
	}

	/// <summary>
	/// Disables and subscribes all valid patrols in each sequence to this EnemyManager and, if it exists, our attached NavMeshAgent.
	/// </summary>
    private void SubscribePatrols()
    {
		foreach (AIPackage p in normalPatrols)
        {
			if (p != null)
			{
				// Patrols will control their own enabling/disabling.
				p.enabled = false;

				p.AttachManager(this);
				p.AttachAgent(_agent);
			}
        }

		foreach (AIPackage p in initialAlertPatrols)
		{
			if (p != null)
			{
				// Patrols will control their own enabling/disabling.
				p.enabled = false;

				p.AttachManager(this);
				p.AttachAgent(_agent);
			}
		}

		foreach (AIPackage p in alertedPatrols)
		{
			if (p != null)
			{
				// Patrols will control their own enabling/disabling.
				p.enabled = false;

				p.AttachManager(this);
				p.AttachAgent(_agent);
			}
		}

		foreach (AIPackage p in searchPatrols)
		{
			if (p != null)
			{
				// Patrols will control their own enabling/disabling.
				p.enabled = false;

				p.AttachManager(this);
				p.AttachAgent(_agent);
			}
		}
    }

	/// <summary>
	/// Subscribes this enemy to player events that affect its patrolling.
	/// </summary>
	public void SubscribeToPlayerEvents()
	{
		DisguiseManager.PlayerDisguiseChange += RestartPatrol;
		PlayerManager.OnEnterOrExit += FollowPlayerTo;

		CancelInvoke("LazyUnsubscribeFromPlayerEvents");
	}

	/// <summary>
	/// Unsubscribes this enemy from player events that affect its patrolling.
	/// </summary>
	public void UnsubscribeFromPlayerEvents()
	{
		DisguiseManager.PlayerDisguiseChange -= RestartPatrol;

		if (playerTrackBuffer > 0f)
		{
			Invoke("LazyUnsubscribeFromPlayerEvents", playerTrackBuffer);
		}
		else
		{
			LazyUnsubscribeFromPlayerEvents();
		}
	}

	/// <summary>
	/// Unsubscribes this enemy from player events that affect its patrolling, later than other unsubscriptions.
	/// </summary>
	public void LazyUnsubscribeFromPlayerEvents()
	{
		PlayerManager.OnEnterOrExit -= FollowPlayerTo;
	}

	/// <summary>
	/// Attempted to enter the provided patrol sequence at the provided index. Failing that, we search the sequence for a valid index.
	/// </summary>
	/// <returns><c>true</c>, if patrol sequence was sucessfully entered at any index, <c>false</c> otherwise.</returns>
	/// <param name="sequence">Patrol sequence to attempt to enter.</param>
	/// <param name="index">The first index at which an attempt is made to enter the sequence.</param>
	public bool EnterPatrolSequence(AIPackage[] sequence, int index)
	{
		index = FindPatrolInSequence(sequence, index);

		if (index != -1)
		{
			_currentSequence = sequence;
			_currentIndex = index;

			_currentSequence[_currentIndex].StartPackage();

			return true;
		}
		else
		{
			//Debug.Log("EnterPatrolSequence (EnemyManager.cs): Invalid patrol index/sequence provided. Sequence was not entered.");
			return false;
		}
	}

	/// <summary>
	/// Searches the provided patrol sequence for a valid patrol, starting at the provided index.
	/// </summary>
	/// <returns>The index of the first valid patrol found in the sequence, or -1 if the whole sequence was searched fruitlessly.</returns>
	/// <param name="sequence">The sequence to search through.</param>
	/// <param name="searchFromIndex">Initial index to search from. This will be clamped to sequence boundaries before the search commences.</param>
	protected int FindPatrolInSequence(AIPackage[] sequence, int searchFromIndex)
	{
		if (sequence.Length == 0)
		{
			//Debug.Log("FindPatrolInSequence (EnemyManager.cs): Input sequence has no AIPackages.");
			return -1;
		}

		// Clamp input index to the sequence's extremes.
		if (searchFromIndex < 0)
			searchFromIndex = 0;
		else if (searchFromIndex > sequence.Length)
			searchFromIndex = sequence.Length - 1;

		int currentSearchIndex = searchFromIndex;

		// Do an initial pass on the clamped input index.
		if (sequence.Length <= currentSearchIndex)
			currentSearchIndex = 0;

		if (sequence[currentSearchIndex] != null)
			return currentSearchIndex;
		else
			currentSearchIndex++;

		while (currentSearchIndex != searchFromIndex)
		{
			if (sequence.Length <= currentSearchIndex)
				currentSearchIndex = 0;

			if (sequence[currentSearchIndex] != null)
				return currentSearchIndex;
			else
				currentSearchIndex++;
		}

		//Debug.Log("FindPatrolInSequence (EnemyManager.cs): No valid AIPackage found in input sequence.");
		return -1;
	}

	/// <summary>
	/// Notifies the manager that the given patrol has ended. If it was the manager's current patrol, we move on to the next patrol in the current sequence.
	/// </summary>
	/// <param name="ended_patrol">The patrol that ended.</param>
	public void NotifyEndPatrol(AIPackage endedPatrol)
    {
		//int nextIndex = 0;

		if (endedPatrol == _currentSequence[_currentIndex])
        {
			//nextIndex = FindPatrolInSequence(_currentSequence, );
            
			//if (nextIndex != -1)

			// Check if the patrol that ended was the final patrol of initialAlertPatrols, a sequence which does not loop.
			//if (initialAlertPatrols.Length > 0 && ended_patrol == initialAlertPatrols[initialAlertPatrols.Length - 1])
			if (_currentSequence == initialAlertPatrols)
			{
				if (initialAlertPatrols.Length == 0)
				{
					AlertSelf();
					return;
				}

				int lastIndex = initialAlertPatrols.Length - 1;

				while (lastIndex > 0 && initialAlertPatrols[lastIndex] == null)
					lastIndex--;

				if (_currentIndex == lastIndex)
					AlertSelf();
				else
					EnterPatrolSequence(_currentSequence, _currentIndex + 1);

				//EnterPatrolSequence(alertedPatrols, 0);
			}
			else
				EnterPatrolSequence(_currentSequence, _currentIndex + 1);
        }
        else
        {
			if (endedPatrol == tempPackage)
			{
				Destroy(tempPackage);
				tempPackage = null;
			}
			else
            	Debug.Log("Patrol " + endedPatrol.name + " broadcast unexpected end notification to enemy object " + gameObject.name + "'s EnemyManager. Attempting to re-enter current patrol.");

			//nextIndex = FindPatrolInSequence(_currentSequence, _currentIndex);

			//if (nextIndex != -1)
			//{
			RestartPatrol();
			EnterPatrolSequence(_currentSequence, _currentIndex);
			//}
        }
    }

	/// <summary>
	/// Forces the enemy to enter a new package outside of their established sequences.
	/// </summary>
	/// <param name="package">New package to enter.</param>
	public void ForceNewPackage(AIPackage package)
	{
        //TODO: Implement real shit.
		if (package != null && tempPackage == null)
		{
			package.enabled = false;

			HaltPatrol();

			package.AttachAgent(_agent);
			package.AttachManager(this);

			tempPackage = package;
			tempPackage.StartPackage();
		}
	}

	/// <summary>
	/// Prints the info of the current AIPackage to the console.
	/// </summary>
	[ContextMenu ("Log Current Package Info")]
	public void LogCurrentPackageInfo()
	{
		if (_currentSequence != null && _currentSequence[_currentIndex] != null)
		{
			_currentSequence[_currentIndex].LogInfo();
		}
	}

	/// <summary>
	/// Prints the info of the most recently interrupted AIPackage to the console.
	/// </summary>
	[ContextMenu ("Log Interrupted Package Info")]
	public void LogInterruptedPackageInfo()
	{
		if (_interruptedSequence != null && _interruptedSequence[_interruptedIndex] != null)
		{
			_interruptedSequence[_interruptedIndex].LogInfo();
		}
	}

	/// <summary>
	/// Start the coroutine representing this enemy's stunned state.
	/// </summary>
    public void StunSelf()
    {
		if (behaviours.isHardy)
			StartCoroutine(Stunned());
		else
			StunEnemyHard();
    }

	/// <summary>
	/// The coroutine representing this enemy's stunned state, and his or her subsequent recovery from it.
	/// </summary>
    private IEnumerator Stunned()
    {
        _isStunned = true;
        if (alertImage != null)
            alertImage.enabled = false;
        if (stunImage != null)
            stunImage.enabled = true;

        HaltPatrol();

		if (leftVisionField != null && rightVisionField != null)
		{
			leftVisionField.SetActive(false);
			rightVisionField.SetActive(false);
		}

		yield return new WaitForSeconds(stunnedTime);

		RecoverFromStun();
    }

	/// <summary>
	/// Stuns the enemy such that they will not recover automatically. Other enemies can wake this enemy up once found.
	/// </summary>
	private void StunEnemyHard()
	{
		_isStunned = true;
		if (alertImage != null)
			alertImage.enabled = false;
		if (stunImage != null)
			stunImage.enabled = true;

		HaltPatrol();

		if (leftVisionField != null && rightVisionField != null)
		{
			leftVisionField.SetActive(false);
			rightVisionField.SetActive(false);
		}
	}

	public void RecoverFromStun()
	{
		if (_isStunned)
		{
			_isStunned = false;
			if (stunImage != null)
				stunImage.enabled = false;

			// The enemy will come out of his or her stunned state alerted.
			//RestartPatrol();
			//EnterPatrolSequence(alertedPatrols, 0);

			//PlayerFound();

            if (anim != null && wakeUpSelfClip != null)
            {
                anim.SetTrigger("wakeUpSelf");

                Invoke("ForceResume", (wakeUpSelfClip.length / 3f) + 0.1f);
            }
            else
			    ForceResume();
		}
	}

	/// <summary>
	/// Interrupt the current patrol sequence. This sequence may or may not be returned to.
	/// </summary>
    private void HaltPatrol()
    {
		AIPackage currentPatrol = _currentSequence[_currentIndex];

		if (tempPackage != null)
		{
			Destroy(tempPackage);
			tempPackage = null;
		}

        if (currentPatrol != null)
        {
            currentPatrol.enabled = false;
            currentPatrol.StopAllCoroutines();

            if (_agent != null)
                _agent.destination = transform.position;

			_interruptedSequence = _currentSequence;
			_interruptedIndex = _currentIndex;
        }
    }

	/// <summary>
	/// Reset necessary aspects of a patrol before entering a new sequence after an interruption.
	/// </summary>
    private void RestartPatrol()
    {
		/*
		if (_interruptedPatrol != null)
			_currentPatrol = _interruptedPatrol;

		if (_currentPatrol != null)
			_currentPatrol.StartPatrol();
		*/

        // Reset our vision field, in case Kaida is still standing in it when the guard stops being stunned.
        if (_isFacingLeft && leftVisionField != null)
        {
            leftVisionField.SetActive(false);
            leftVisionField.SetActive(true);
        }
        else if (!_isFacingLeft && rightVisionField != null)
        {
            rightVisionField.SetActive(false);
            rightVisionField.SetActive(true);
        }
    }

	/// <summary>
	/// Forces the enemy to resume his or her most recently interrupted patrol.
	/// </summary>
	public void ForceResume()
	{
		if (_interruptedSequence != null)
		{
			RestartPatrol();

			EnterPatrolSequence(_interruptedSequence, _interruptedIndex);

			if (_currentSequence == alertedPatrols)
			{
				StartCoroutine(TimedSearch());
			}
		}
	}

	//TODO: Pass in a generated empty game object representing Kaida's last known location.
	/// <summary>
	/// Stop the enemy's current patrol sequence and enter the patrol sequence representing his or her initial alerted state,
	/// generally where an attempt is made to alert other enemies.
	/// </summary>
	public void PlayerFound()
	{
		if (!_isStunned)
		{
			// If the enemy is aggressive, or if "finding" has occured after the enemy was already alerted, then we enter fight or flight.
			if (behaviours.isAggressive || (_isAlerted && _currentSequence == alertedPatrols))
			{
				/*
				if (leftVisionField != null && rightVisionField != null)
				{
					leftVisionField.SetActive(false);
					rightVisionField.SetActive(false);
				}
				*/

				if (searchRoutine != null)
					StopCoroutine(searchRoutine);

				// TODO: Implement a fight or flight sequence.
			}
			else
			{
				if (alertImage != null)
					alertImage.enabled = true;

				if (leftVisionField != null && rightVisionField != null)
				{
					leftVisionField.SetActive(false);
					rightVisionField.SetActive(false);
				}

				HaltPatrol();

				if (!EnterPatrolSequence(initialAlertPatrols, 0))
					AlertSelf();
			}
		}
	}

	/// <summary>
	/// Receive an external alert and become alerted if this EnemyManager was not the source.
	/// </summary>
	/// <param name="alertOrigin">The EnemyManager from which the alert originated.</param>
	public void ReceiveAlert(EnemyManager alertOrigin)
	{
		if (alertOrigin != this)
		{
			//Debug.Log(gameObject.name + " received alert from " + alertOrigin.gameObject.name);

			if (behaviours.isTactical && !_isStunned)
			{
				HaltPatrol();

				if (EnterPatrolSequence(initialAlertPatrols, 0))
					return;
			}

			AlertSelf();
		}
	}

	/// <summary>
	/// Stop the enemy's current patrol sequence and enter the patrol sequence representing his or her alerted state.
	/// </summary>
    public void AlertSelf()
    {
		if (!_isStunned)
        {
            if (alertImage != null)
                alertImage.enabled = true;

			_isAlerted = true;

            HaltPatrol();

			if (EnterPatrolSequence(alertedPatrols, 0))
			{
				searchRoutine = TimedSearch();
				StartCoroutine(searchRoutine);
			}
			else
				ForceResume();
        }
    }

	/// <summary>
	/// Sets the target location(s) that this guard will move to in pursuit of the player.
	/// </summary>
	/// <param name="targetLoc">Target location.</param>
	private void FollowPlayerTo(Transform targetLoc)
	{
		if (targetLoc != null)
		{
			//Debug.Log("follow set");

			if (followToLoc == null)
			{
				followToLoc = targetLoc;

				//Debug.Log("follow set initial");
			}
			else
			{
				followLocBacklog.Add(targetLoc);

				//Debug.Log("follow set backlog");
			}

			// We reset the enemy's collider, in case it's already inside of the door/alcove trigger zone when Kaida moves.
			Collider col = GetComponent<Collider>();
			if (col != null)
			{
				col.enabled = false;
				col.enabled = true;
			}
		}
	}

	/// <summary>
	/// Follows the player to the next logged location, and shifts the backlog up.
	/// </summary>
	public void FollowNext()
	{
		//Debug.Log("follow");

		if (followToLoc != null)
		{
			//Debug.Log("following");

			Transform temp = followToLoc;

			followToLoc = null;

			if (followLocBacklog.Count > 0)
			{
				followToLoc = followLocBacklog[0];
				followLocBacklog.RemoveAt(0);
			}

			hardZ = temp.position.z;
			transform.position = new Vector3(temp.position.x, temp.position.y, temp.position.z);
		}
	}

	/// <summary>
	/// Wipes the stored target follow locations from memory. To be called when the player reappears before the guard.
	/// </summary>
	public void WipeFollow()
	{
		followToLoc = null;
		followLocBacklog.Clear();

		//Debug.Log("follow wiped");
	}

	/// <summary>
	/// If this countdown concludes successfully, the enemy will go back off alert.
	/// </summary>
	private IEnumerator TimedSearch()
	{
		yield return new WaitForSeconds(timeToSearch);

		if (!_isStunned)
		{
			StopAlert();
		}
	}

	private void RefreshSearchTimer()
	{
		if (searchRoutine != null)
		{
			StopCoroutine(searchRoutine);
			StartCoroutine(searchRoutine);
		}
	}

	/// <summary>
	/// Exit the enemy's alerted state and re-enter his or her normal patrol sequence.
	/// </summary>
	public void StopAlert()
	{
		if (_currentSequence == alertedPatrols)
		{
			_isAlerted = false;

			if (alertImage != null)
				alertImage.enabled = false;

			HaltPatrol();

			RestartPatrol();
			EnterPatrolSequence(normalPatrols, 0);
		}
	}

	/// <summary>
	/// Turn the enemy left or right, updating vision cones and other enemy features accordingly.
	/// </summary>
	/// <param name="turningLeft">If set to <c>true</c>, the enemy turns left. Otherwise, the enemy turns right.</param>
    public void Turn(bool turningLeft)
    {
        if (_isFacingLeft != turningLeft)
        {
            _isFacingLeft = !_isFacingLeft;

            //transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
            if (_renderer != null)
                _renderer.flipX = _isFacingLeft;

			// TODO: Make better algorithm for when it's appropriate to swap vision cones.
			if (leftVisionField != null && rightVisionField != null && _currentSequence != initialAlertPatrols)
            {
                if (_isFacingLeft)
                {
                    leftVisionField.SetActive(true);
                    rightVisionField.SetActive(false);
                }
                else
                {
                    leftVisionField.SetActive(false);
                    rightVisionField.SetActive(true);
                }
            }
        }
    }

	public void Unsubscribe()
	{
		EnemyObserver.AlertAllOtherEnemies -= ReceiveAlert;
	}

	void OnDestroy()
	{
		Unsubscribe();
		EnemyObserver.enemies.Remove(this);
	}
}

[System.Serializable]
public struct EnemyPersonalityTraits
{
	/// <summary>
	/// A hardy enemy will only be stunned temporarily.
	/// </summary>
	public bool isHardy;

	/// <summary>
	/// A tactical enemy, upon receiving an alert from a fellow enemy, will enter their initial alert sequence rather than their alerted loop.
	/// </summary>
	public bool isTactical;

	/// <summary>
	/// An aggressive enemy, upon finding Kaida, will enter their alerted loop rather than their initial alert sequence.
	/// </summary>
	public bool isAggressive;
}
