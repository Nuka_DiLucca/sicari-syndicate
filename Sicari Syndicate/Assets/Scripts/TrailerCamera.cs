﻿using UnityEngine;
using System.Collections;

public class TrailerCamera : MonoBehaviour {

    public Transform startPoint;
    public Transform endPoint;
    public float modifier = .05f;
    private float t;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        this.gameObject.transform.position = new Vector3(Mathf.Lerp(startPoint.position.x, endPoint.position.x, t), Mathf.Lerp(startPoint.position.y, endPoint.position.y, t), Mathf.Lerp(startPoint.position.z, endPoint.position.z, t));
        t += Time.deltaTime * modifier;


    }
}
