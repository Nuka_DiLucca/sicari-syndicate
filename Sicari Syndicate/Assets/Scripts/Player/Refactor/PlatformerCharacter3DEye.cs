using System;
using UnityEngine;

/// <summary>
/// Script Template: Unity Technologies
/// Editor: Michael DiLucca
/// As of 10/27/2016 This script will be used for collision detection for the player controller
/// Please refer to this script when dealing with any and all player to character interactions
/// on the level of controlls and behaviors.
/// PLEASE NOTE: PlatformerCharacter2D IS CURRENTLY OBSOLETE!!!
/// </summary>

public class PlatformerCharacter3DEye : PlatformerCharacter3D {

	//[SerializeField]
    //private float m_JumpForce = 400f;                  // Amount of force added when the player jumps.

    private Transform m_AirCheckSW;
    private Transform m_AirCheckNE;
    private Transform m_AirCheckSE;
    private Transform m_AirCheckNW;

    private bool m_SWGrounded;
    private bool m_NEGrounded;
    private bool m_SEGrounded;
    private bool m_NWGrounded;

	protected void Awake() {
        // Setting up references.
		base.Awake ();
		m_RunSpeed = 2f;
		m_MaxSpeed = 2f;
		colMod = 6f;
        m_AirCheckSW = transform.Find("AirCheckSW");
        m_AirCheckNE = transform.Find("AirCheckNE");
        m_AirCheckSE = transform.Find("AirCheckSE");
        m_AirCheckNW = transform.Find("AirCheckNW");

        BatteryManager.OnBatteryEmpty += EyeForceDrop;
    }

	protected override void FixedUpdate() {

		base.FixedUpdate ();

        m_SWGrounded = false;
        m_NEGrounded = false;
        m_SEGrounded = false;
        m_NWGrounded = false;
        m_Rigidbody.useGravity = true;

        Collider[] swColliders = Physics.OverlapSphere(m_AirCheckSW.position, k_GroundedRadius / colMod, m_WhatIsGround);
        for (int i = 0; i < swColliders.Length; i++) {
            if (swColliders[i].gameObject != gameObject)
                m_SWGrounded = true;
        }

        Collider[] neColliders = Physics.OverlapSphere(m_AirCheckNE.position, k_GroundedRadius / colMod, m_WhatIsGround);
        for (int i = 0; i < neColliders.Length; i++) {
			if (neColliders [i].gameObject != gameObject) {
				m_NEGrounded = true;
				m_Rigidbody.useGravity = false;
				print (neColliders [i].gameObject.name);
			}
        }

        Collider[] seColliders = Physics.OverlapSphere(m_AirCheckSE.position, k_GroundedRadius / colMod, m_WhatIsGround);
        for (int i = 0; i < seColliders.Length; i++) {
            if (seColliders[i].gameObject != gameObject)
                m_SEGrounded = true;
        }

        Collider[] nwColliders = Physics.OverlapSphere(m_AirCheckNW.position, k_GroundedRadius / colMod, m_WhatIsGround);
        for (int i = 0; i < nwColliders.Length; i++) {
			if (nwColliders [i].gameObject != gameObject) {
				m_NWGrounded = true;
				m_Rigidbody.useGravity = false;
			}
        }
    }

    public override void Move(float horMove, float verMove, bool jump) {

        //only control the player if grounded or airControl is turned on
        if (m_Grounded) {
			
                Vector2 vel = new Vector2(horMove * m_MaxSpeed, verMove * m_MaxSpeed);

                if (m_SWGrounded && m_SEGrounded) 
                    vel = new Vector2(vel.x, -.5f);
                if (m_NWGrounded && m_NEGrounded) 
                    vel = new Vector2(vel.x, verMove * .5f);
			
                m_Rigidbody.velocity = vel;
        }

        /*// If the player should jump...
        if (m_Grounded && jump) {
            // Add a vertical force to the player.
            m_Grounded = false;
            m_Rigidbody.AddForce(new Vector2(0f, m_JumpForce));
        }*/
    }

	private void EyeForceDrop() {
		m_Rigidbody.useGravity = true;
		m_Rigidbody.AddForce (0, -5f, 0);
	}

    void OnDestroy() {
        BatteryManager.OnBatteryEmpty -= EyeForceDrop;
    }
}
