﻿using UnityEngine;
using System.Collections;

public class RecallSphere : MonoBehaviour
{
	public LimbManager myLimbManager;

	void OnTriggerEnter(Collider other)
	{
		// If the offending object is a limb, i.e. on the Limb layer.
		if (other.gameObject.layer == 8)
		{
			other.gameObject.SetActive (false);
			//print (other.gameObject.name);

			if (myLimbManager != null && other.gameObject.name.ToLower() == "hand")
			{
				myLimbManager.HandleHandPickup();
			}
			else if (myLimbManager != null &&  other.gameObject.name.ToLower() == "eye")
			{
				myLimbManager.HandleEyePickup();
			}
		}
	}
}
