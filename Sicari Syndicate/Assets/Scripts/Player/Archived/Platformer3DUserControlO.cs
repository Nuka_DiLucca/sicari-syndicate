using System;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

/// <summary>
/// Script Template: Unity Technologies
/// Editor: Michael DiLucca
/// As of 10/27/2016 This script will be used for collision detection for the player controller
/// Please refer to this script when dealing with any and all player to character interactions
/// on the level of controlls and behaviors.
/// PLEASE NOTE: Platformer2DUserControl IS CURRENTLY OBSOLETE!!!
/// </summary>

[RequireComponent(typeof(PlatformerCharacter3D))]
public class Platformer3DUserControlO : MonoBehaviour
{
	/*public AudioClip movementSound;
	public AudioClip stunSound;
	public AudioClip scanSound;
	public AudioClip miscSound;
	public AudioClip swapToDisguise;
	public AudioClip endDisguise;

    private PlatformerCharacter3D m_Character;
	private Rigidbody myRigidbody;
    private guardAnimation guardAnimControl;
    private SpriteRenderer myRenderer;
    private bool m_Jump;
	public float stunBatteryUsage = 30f;
    public DisguiseManager disguiseManager;
	public Image win;							// Temporary.
	public Image loss;							// Temporary.
	public bool isMainControl = false;

    // Move into a background structure and manipulate the colliders accordingly
    private bool canEnter = false;
    private bool canInteract = false;
    private bool canExit = false;
	private Transform moveToOnInteract;

	private GameObject myCamera;
	private float timeToStep = 0f;
	public AudioSource movementSoundPlayer;
	public AudioSource actionSoundPlayer;

    private void Awake()
	{
        BatteryManager.OnBatteryEmpty += DeactivateGuardControls;

		m_Character = GetComponent<PlatformerCharacter3D>();
		myRigidbody = GetComponent<Rigidbody>();
        guardAnimControl = gameObject.GetComponent<guardAnimation>();
        myRenderer = gameObject.GetComponent<SpriteRenderer>();
    }

	private void Start()
    {
        if (DisguiseManager.primaryManager != null)
        {
            disguiseManager = DisguiseManager.primaryManager;
        }
        else
            disguiseManager = gameObject.GetComponent<DisguiseManager>();

		if (myCamera == null)
		{
			if (isMainControl)
				myCamera = PlayerManager.playerCamera;
			else if (m_Character.isHand)
				myCamera = PlayerManager.handCamera;
			else if (m_Character.isEye)
				myCamera = PlayerManager.eyeCamera;
		}

		if (myCamera != null)
		{
			movementSoundPlayer = myCamera.AddComponent<AudioSource>();
			movementSoundPlayer.volume = 0.25f;
			actionSoundPlayer = myCamera.AddComponent<AudioSource>();
			actionSoundPlayer.volume = 0.5f;
		}
		else
			Debug.Log("No camera for " + gameObject.name);
    }

    private void Update()
	{
        if (!m_Jump)
		{
            // Read the jump input in Update so button presses aren't missed.
            //m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
        }

        if (canEnter)
		{
            if (Input.GetButtonDown("Enter") && canEnter && moveToOnInteract != null)
			{
				//TODO: Implement a y-offset to the destination position for drones.

                canEnter = false;
                //transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, moveToOnInteract.position.z);
				transform.position = new Vector3(moveToOnInteract.position.x, moveToOnInteract.position.y, moveToOnInteract.position.z);

				EnterExitTriggerZone zone = moveToOnInteract.GetComponent<EnterExitTriggerZone>();

				if (zone != null)
				{
					zone.DisableObjects();
				}

                print("entered");
            }
        }

        if (canExit)
		{ 
			if (Input.GetButtonDown("Exit") && !canEnter && moveToOnInteract != null)
			{
				//TODO: Implement a y-offset to the destination position for drones.

                canEnter = true;
                //transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, moveToOnInteract.position.z);
				transform.position = new Vector3(moveToOnInteract.position.x, moveToOnInteract.position.y, moveToOnInteract.position.z);

				EnterExitTriggerZone zone = moveToOnInteract.GetComponent<EnterExitTriggerZone>();

				if (zone != null)
				{
					zone.EnableObjects();
				}

                print("exited");
            }
        }

        // VERY temporary stun function until this all gets reimplemented on proper character controllers.
        if (Input.GetButtonDown("Jump"))
		{
            Collider[] hits = Physics.OverlapSphere(transform.position, 0.3f);

            for (int i = 0; i < hits.Length; i++)
			{
                EnemyManager enemyScript = hits[i].gameObject.GetComponent<EnemyManager>();

                if (enemyScript != null)
				{
					if (!enemyScript.IsStunned && !m_Character.isEye)
					{
						if (BatteryManager.primaryBatteryManager != null && BatteryManager.primaryBatteryManager.currentBattery > 0f)
						{
							if (stunBatteryUsage < 0f)
								stunBatteryUsage = 0f;

							BatteryManager.primaryBatteryManager.UseBatteryImmediate(stunBatteryUsage);

                            PlayActionSound(stunSound);

                            enemyScript.StunSelf();
                            break;
                        }
                    }
					else if (enemyScript.IsStunned && disguiseManager != null && !m_Character.isHand)
					{
                        Debug.Log("Add disguise attempt.");

                        SpriteRenderer newRend = enemyScript.gameObject.GetComponent<SpriteRenderer>();
                        Animator newAnim = enemyScript.gameObject.GetComponent<Animator>();

                        if (newRend != null && newAnim != null)
						{
                            //Disguise newDisguise = new Disguise(newRend.sprite, newAnim.runtimeAnimatorController, enemyScript.tagsInDisguise);
                            Disguise newDisguise = new Disguise(enemyScript.disguiseSprite, newAnim.runtimeAnimatorController, enemyScript.tagsInDisguise);
                            if (disguiseManager.AddDisguise(newDisguise))
								PlayActionSound(scanSound);
                        }
                    }
                }
            }
        }

		if (disguiseManager != null && isMainControl)
		{
            if (Input.GetKeyDown(KeyCode.Alpha1))
			{
                Debug.Log("Swap attempt: disguise 1.");
                if (disguiseManager.SwapToDisguise(0))
                {
                    PlayActionSound(swapToDisguise);

                    ActivateGuardControls();
                }
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2))
			{
                Debug.Log("Swap attempt: disguise 2.");
                if (disguiseManager.SwapToDisguise(1))
                {
                    PlayActionSound(swapToDisguise);

                    ActivateGuardControls();
                }
            }
            else if (Input.GetKeyDown(KeyCode.Alpha3))
			{
                Debug.Log("Swap attempt: disguise 3.");
                if (disguiseManager.SwapToDisguise(3))
                {
                    PlayActionSound(swapToDisguise);

                    ActivateGuardControls();
                }
            }
            else if (Input.GetKeyDown(KeyCode.R))
			{
                Debug.Log("Disguise reset attempt.");
                if (disguiseManager.EndDisguise())
                {
                    PlayActionSound(endDisguise);

                    DeactivateGuardControls();
                }
            }
        }
    }

    /// <summary>
    /// Activate the guard control/animation scheme.
    /// </summary>
    private void ActivateGuardControls()
    {
        if (guardAnimControl != null)
        {
            guardAnimControl.enabled = true;
        }
    }

    /// <summary>
    /// Deactivate the guard control/animation scheme.
    /// </summary>
    private void DeactivateGuardControls()
    {
        if (guardAnimControl != null)
        {
            guardAnimControl.enabled = false;

            if (myRenderer != null)
                myRenderer.flipX = false;
        }
    }

    private void FixedUpdate()
	{
        // Read the inputs.
        float h = CrossPlatformInputManager.GetAxis("Horizontal");
        float v = CrossPlatformInputManager.GetAxis("Vertical");
        // Pass all parameters to the character control script.
        m_Character.Move(h, v, m_Jump);
        m_Jump = false;

		float speed = Mathf.Abs(myRigidbody.velocity.x);

		if (speed != 0f)
		{
			timeToStep += Time.fixedDeltaTime;

			if (timeToStep > ((5f - speed) / 5f))
			{
				if (movementSoundPlayer != null && movementSound != null)
					movementSoundPlayer.PlayOneShot(movementSound);

				timeToStep = 0f;
			}
		}
		else
			timeToStep = 0f;
    }

	public void PlayActionSound(AudioClip sound)
	{
		if (actionSoundPlayer != null && sound != null)
		{
			actionSoundPlayer.PlayOneShot(sound);
		}
	}

	/*
    void OnTriggerEnter(Collider other) {
        print(other.name);

        if (other.tag == "TriggerZone1") {
            canEnter = true;
            canExit = false;
        }

        if (other.tag == "TriggerZone2") {
            canEnter = false;
            canExit = true;
        }
    }

    void OnTriggerExit(Collider other) {
        canExit = false;
        canEnter = false;    
    }


	public void SetCanEnter(bool canNowEnter)
	{
		canEnter = canNowEnter;
	}

	public void SetCanExit(bool canNowExit)
	{
		canExit = canNowExit;
	}

	public void SetDestination(Transform destination)
	{
		moveToOnInteract = destination;
	}

    void OnDestroy()
    {
        BatteryManager.OnBatteryEmpty -= DeactivateGuardControls;
    }*/
}

