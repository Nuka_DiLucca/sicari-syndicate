﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NavAgentController : MonoBehaviour
{
	public DisguiseManager disguiseManager;
    private Animator kaidaAnim;
	public Image win;						// Temporary.
	public Image loss;						// Temporary.

    private NavMeshAgent _myAgent;
    private SpriteRenderer _myRenderer;

	void Awake()
    {
		disguiseManager = gameObject.GetComponent<DisguiseManager>();

        _myAgent = gameObject.GetComponent<NavMeshAgent>();
        kaidaAnim = GetComponent<Animator>();
        _myRenderer = gameObject.GetComponent<SpriteRenderer>();

        if (_myAgent != null)
            _myAgent.updateRotation = false;
	}
	
	void Update()
    {
        if (_myAgent != null)
        {
            if (Input.GetButton("Horizontal"))
            {
                float xAxis = Input.GetAxis("Horizontal");
                Vector3 newDest = new Vector3(transform.position.x + 10 * xAxis, transform.position.y, transform.position.z);

                _myAgent.destination = newDest;

              

            }
            else
                _myAgent.destination = transform.position;

            // VERY temporary stun function until this all gets reimplemented on proper character controllers.
            if (Input.GetButtonDown("Jump"))
            {
                Collider[] hits = Physics.OverlapSphere(transform.position, 0.4f);

                for (int i = 0; i < hits.Length; i++)
                {
                    EnemyManager enemyScript = hits[i].gameObject.GetComponent<EnemyManager>();

                    if (enemyScript != null)
                    {
						if (!enemyScript.IsStunned)
						{
							enemyScript.StunSelf();
							break;
						}
						else if (enemyScript.IsStunned && disguiseManager != null)
						{
							Debug.Log("Add disguise attempt.");

							SpriteRenderer newRend = enemyScript.gameObject.GetComponent<SpriteRenderer>();
							Animator newAnim = enemyScript.gameObject.GetComponent<Animator>();

							if (newRend != null && newAnim != null)
							{
								Disguise newDisguise = new Disguise(newRend.sprite, newAnim.runtimeAnimatorController, enemyScript.tagsInDisguise);
								disguiseManager.AddDisguise(newDisguise);
							}
						}
                    }
                }
            }

			if (disguiseManager != null)
			{
				if (Input.GetKeyDown(KeyCode.Alpha1))
				{
					Debug.Log("Swap attempt: disguise 1.");
					disguiseManager.SwapToDisguise(0);
				}
				else if (Input.GetKeyDown(KeyCode.Alpha2))
				{
					Debug.Log("Swap attempt: disguise 2.");
					disguiseManager.SwapToDisguise(1);
				}
				else if (Input.GetKeyDown(KeyCode.Alpha3))
				{
					Debug.Log("Swap attempt: disguise 3.");
					disguiseManager.SwapToDisguise(2);
				}
				else if (Input.GetKeyDown(KeyCode.R))
				{
					Debug.Log("Disguise reset attempt.");
					disguiseManager.EndDisguise();
				}
			}
        }
	}
}
