﻿using UnityEngine;
using System.Collections;

public class Hookshot : MonoBehaviour
{

    public KeyCode myKey;                               //What key we press to hookshot
	public AudioClip hookSound;
    public Vector2 hookAngle = new Vector2(1, 0.5f);
    public Vector2 hook = new Vector2();           
    public bool shoot = true;                          //for shooting left and right. Set this to true or start the object against the wall if you want to start shooting to the left
    public float waitTime = 1f;                         //how long to wait  between shots
    public bool grappling = false;                      //true when we're grappling
	private bool inAir = false;							//true when in mid air
    public TrailRenderer trail;
    private float startTime;
    public Vector3 startMarker;
    public Vector3 endMarker;
    private float journeyLength;


	private Platformer3DUserControl myController;

    //Speed Variables
    public float hookSpeed = 1f;                        //how fast we move while hookshotting
    public float multiplier = 1.3f;                     //how much we speed up/slow down by

	// Audio
	/*public AudioSource myAudio;
	public AudioSource myAudio2;
	public AudioClip grapple;
	public AudioClip landing1;
	public AudioClip landing2;*/

	//Hitpause
	public float hitPauseLength = .1f;

    //Set our left angle, audio, and sprite
    void Start () {
		myController = gameObject.GetComponent<Platformer3DUserControl>();

        trail.gameObject.SetActive(false);
        hook = new Vector2(-hookAngle.x, hookAngle.y);
        //myAudio = GetComponent<AudioSource>();
    }

    //OnCollisionEnter we're no longer grappling and we change shot direction
    void OnCollisionEnter(Collision collision) {
		
	}

    //If we're grappling to something, keep going that way unless we press the grapple button again, in which case we die.
    //If we're not grappling, keep us climbing slowly and check to make sure we're still on screen + alive
    void Update () {     

        if (Input.GetKeyDown (myKey) && myController.enabled) {
			if (shoot) {

                RaycastHit hit;
				if (Physics.Raycast(transform.position, transform.up, out hit)) {

                    Vector3 outVec = new Vector3(hit.point.x, hit.point.y + .01f, transform.localPosition.z);
                    trail.gameObject.SetActive(true);
                    trail.transform.position = outVec;
                    grappling = true;
                    inAir = true;
                    startTime = Time.time;
                    startMarker = new Vector3(transform.position.x, transform.position.y, transform.localPosition.z);
                    endMarker = outVec;
                    journeyLength = Vector2.Distance(startMarker, endMarker);

					PlayerManager.currentController.PlayActionSound(hookSound);
                }      
            }
        }

        if (grappling) {
            float distCovered = (Time.time - startTime) * hookSpeed;
            float fracJourney = distCovered / journeyLength;
            transform.position = Vector3.Lerp(startMarker, endMarker, fracJourney);
            if (new Vector3(transform.position.x, transform.position.y, transform.localPosition.z) == endMarker) {
                grappling = false;
                inAir = false;
            }
        }
    }

	IEnumerator HitPause() {
		Time.timeScale = 0;
		yield return new WaitForSeconds (hitPauseLength);
		Time.timeScale = 1;
		CameraShake.Shake ();
	}
}
