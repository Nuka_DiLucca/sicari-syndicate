using System;
using UnityEngine;

/// <summary>
/// Script Template: Unity Technologies
/// Editor: Michael DiLucca
/// As of 10/27/2016 This script will be used for collision detection for the player controller
/// Please refer to this script when dealing with any and all player to character interactions
/// on the level of controlls and behaviors.
/// PLEASE NOTE: PlatformerCharacter2D IS CURRENTLY OBSOLETE!!!
/// </summary>

public class PlatformerCharacter3DHand : PlatformerCharacter3D {

    //private float                     // The fastest the player can travel in the x axis.
    //[SerializeField]
    //private float m_JumpForce = 20f;               // Amount of force added when the player jumps.

	protected override void Awake() {
		// Setting up references.
		base.Awake ();
		m_MaxSpeed = 1.5f;
		m_RunSpeed = 1.5f;
	}
}
