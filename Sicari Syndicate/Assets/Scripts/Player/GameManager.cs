﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    public float numPlayers;
    public static GameManager instance { get; set; }

    public Text winText;
    public Button main;

    void Awake() {
        instance = this;
    }

	void Update () {
	
        if (numPlayers <= 0) 
            Application.LoadLevel("GameOver");

        if(numPlayers == 1) {
			
            winText.gameObject.SetActive(true);
            main.gameObject.SetActive(true);
            Time.timeScale = 0;
        }
	}
}
