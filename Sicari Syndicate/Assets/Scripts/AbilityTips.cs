﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AbilityTips : MonoBehaviour {

    public GameObject myTip;
	private Text myTipText;
	[TextAreaAttribute(1, 10)]
	public string tipText;

	// Use this for initialization
	void Start ()
    {
        myTip.SetActive(false);
		myTipText = myTip.transform.FindChild ("Text").GetComponent<Text>();
	}
	
	// Update is called once per frame
	void OnTriggerEnter(Collider myColl)
    {
        if (myColl.gameObject.layer == 8 || myColl.gameObject.layer == 9)
        {
            myTip.SetActive(true);
			myTipText.text = tipText;

        }
    }
    void OnTriggerExit(Collider myColl)
    {
        if (myColl.gameObject.layer == 8 || myColl.gameObject.layer == 9)
        {
            myTip.SetActive(false);
			myTipText.text = " ";
        }
    }
}
