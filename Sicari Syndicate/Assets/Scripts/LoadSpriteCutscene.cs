﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class LoadSpriteCutscene : MonoBehaviour {

	public string sceneToLoad;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider myColl)
	{
		if (myColl.gameObject.layer == 9) 
		{
			SceneManager.LoadScene (sceneToLoad);
		}
	}
}