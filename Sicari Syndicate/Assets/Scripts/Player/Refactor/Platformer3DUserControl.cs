using System;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

/// <summary>
/// Script Template: Unity Technologies
/// Editor: Michael DiLucca
/// As of 10/27/2016 This script will be used for collision detection for the player controller
/// Please refer to this script when dealing with any and all player to character interactions
/// on the level of controlls and behaviors.
/// PLEASE NOTE: Platformer2DUserControl IS CURRENTLY OBSOLETE!!!
/// </summary>

[RequireComponent(typeof(PlatformerCharacter3D))]
public class Platformer3DUserControl : MonoBehaviour
{
	public AudioClip movementSound;
	public AudioClip miscSound;
	public AudioClip stunSound;
	public AudioClip scanSound;
	public AudioClip swapToDisguise;
	public AudioClip endDisguise;
	public AudioSource movementSoundPlayer;
	public AudioSource actionSoundPlayer;

	public DisguiseManager disguiseManager;
	public float stunBatteryUsage = 50f;

    private PlatformerCharacter3D m_Character;
	protected Rigidbody myRigidbody;
    protected SpriteRenderer myRenderer;
    private bool m_Jump;
	public bool isMainControl = false;

    // Move into a background structure and manipulate the colliders accordingly
	protected bool canEnter = false;
	protected bool canInteract = false;
	protected bool canExit = false;
	protected Transform moveToOnInteract;

	protected Camera myCamera;
	private float timeToStep = 0f;

    protected virtual void Awake() {
			
		m_Character = GetComponent<PlatformerCharacter3D>();
		myRigidbody = GetComponent<Rigidbody>();
        myRenderer 	= gameObject.GetComponent<SpriteRenderer>();
    }

	public virtual void Start() {

		myCamera = Camera.main;
			
		movementSoundPlayer = myCamera.gameObject.AddComponent<AudioSource>();
		movementSoundPlayer.volume = 0.25f;
		actionSoundPlayer = myCamera.gameObject.AddComponent<AudioSource>();
		actionSoundPlayer.volume = 0.5f;
    }

	protected virtual void Update() {

		// Read the jump input in Update so button presses aren't missed.
        //if (!m_Jump)			
        //	m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");

        if (canEnter) {
            if (Input.GetButtonDown("Enter") && canEnter && moveToOnInteract != null) {
				//TODO: Implement a y-offset to the destination position for drones.

                canEnter = false;

				transform.position = new Vector3(moveToOnInteract.position.x, moveToOnInteract.position.y, moveToOnInteract.position.z);
				EnterExitTriggerZone zone = moveToOnInteract.GetComponent<EnterExitTriggerZone>();

				if (zone != null)
					zone.DisableObjects();

				PlayerManager.RaiseOnEnterOrExit(moveToOnInteract);
            }
        }

        if (canExit) { 
			if (Input.GetButtonDown("Exit") && !canEnter && moveToOnInteract != null) {
				//TODO: Implement a y-offset to the destination position for drones.

                canEnter = true;

				transform.position = new Vector3(moveToOnInteract.position.x, moveToOnInteract.position.y, moveToOnInteract.position.z);
				EnterExitTriggerZone zone = moveToOnInteract.GetComponent<EnterExitTriggerZone>();

				if (zone != null)
					zone.EnableObjects();

				PlayerManager.RaiseOnEnterOrExit(moveToOnInteract);
            }
        }
	}

	protected virtual void FixedUpdate() {
		
        // Read the inputs.
        float h = CrossPlatformInputManager.GetAxis("Horizontal");
        float v = CrossPlatformInputManager.GetAxis("Vertical");
        // Pass all parameters to the character control script.
        m_Character.Move(h, v, m_Jump);
        //m_Jump = false;

		float speed = Mathf.Abs(myRigidbody.velocity.x);

		if (speed != 0f)
		{
			timeToStep += Time.fixedDeltaTime;

			if (timeToStep > ((3.7f - speed) / 5f))
			{
				if (movementSoundPlayer != null && movementSound != null) {
					movementSoundPlayer.pitch = UnityEngine.Random.Range (1f, 1.5f);
					movementSoundPlayer.PlayOneShot (movementSound);
				}

				timeToStep = 0f;
			}
		}
		else
			timeToStep = 0f;
    }

	public void PlayActionSound(AudioClip sound) {
		if (actionSoundPlayer != null && sound != null) {
			
			//actionSoundPlayer.pitch = Random.range (-.5, 1.5);
			actionSoundPlayer.PlayOneShot(sound);
		}
	}

	public void SetCanEnter(bool canNowEnter) {
		
		canEnter = canNowEnter;
	}

	public void SetCanExit(bool canNowExit) {
		
		canExit = canNowExit;
	}

	public void SetDestination(Transform destination) {
		
		moveToOnInteract = destination;
	}
}

