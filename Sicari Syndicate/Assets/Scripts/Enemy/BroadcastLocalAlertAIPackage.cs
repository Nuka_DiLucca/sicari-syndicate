﻿using UnityEngine;
using System.Collections;

public class BroadcastLocalAlertAIPackage : AIPackage
{
	public float radius;

	public override void StartPackage()
	{
		if (radius > 0f)
		{
			Collider[] hits = Physics.OverlapSphere(transform.position, radius);

			for (int i = 0; i < hits.Length; i++)
			{
				EnemyManager enemyScript = hits[i].gameObject.GetComponent<EnemyManager>();

				if (enemyScript != null)
				{
					enemyScript.ReceiveAlert(manager);
				}
			}
		}
		else
			Debug.Log("Radius of 0 passed to BroadcastLocalAlertAIPackage.");

		EndPackage();
	}
		
	public override void LogInfo()
	{
		Debug.Log("LogInfo: This is a Broadcast Local Alert AI Package.\n" +
			"Radius: " + radius);
	}
}
