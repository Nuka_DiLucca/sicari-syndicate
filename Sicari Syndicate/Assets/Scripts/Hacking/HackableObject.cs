﻿using UnityEngine;
using System.Collections;

public abstract class HackableObject : MonoBehaviour
{
	protected bool isHacked;
	public AudioSource sound;

	public abstract void Hack();
}
