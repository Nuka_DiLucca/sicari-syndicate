﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

    private bool isRight;

	// Update is called once per frame
	void Update () {

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (ray.direction.x >= 0) {
            transform.parent.localScale = new Vector3(Mathf.Abs(transform.parent.localScale.x), transform.parent.localScale.y, transform.parent.localScale.z);
            transform.localPosition = new Vector3(ray.direction.x, ray.direction.y+.4f, transform.localPosition.z);
        } if (ray.direction.x <= 0) {
            transform.parent.localScale = new Vector3(Mathf.Abs(transform.parent.localScale.x), transform.parent.localScale.y, transform.parent.localScale.z);
            transform.localPosition = new Vector3(ray.direction.x, ray.direction.y+.4f, transform.localPosition.z);
        }
    }  
}

