using System;
using UnityEngine;

/// <summary>
/// Script Template: Unity Technologies
/// Editor: Michael DiLucca
/// As of 10/27/2016 This script will be used for collision detection for the player controller
/// Please refer to this script when dealing with any and all player to character interactions
/// on the level of controlls and behaviors.
/// PLEASE NOTE: PlatformerCharacter2D IS CURRENTLY OBSOLETE!!!
/// </summary>

public class PlatformerCharacter3DO : MonoBehaviour {
    /*[SerializeField]
    private float m_MaxSpeed = 3f;                    // The fastest the player can travel in the x axis.
    [SerializeField]
    private float m_RunSpeed = 3f;                    // The fastest the player can travel in the x axis.
    [SerializeField]
    private float m_WalkSpeed = 1.5f;                    // The fastest the player can travel in the x axis.
    [SerializeField]
    private float m_SneakSpeed = 0.8f;                    // The fastest the player can travel in the x axis.
    [SerializeField]
    private float m_JumpForce = 400f;                  // Amount of force added when the player jumps.
    [SerializeField]
    private LayerMask m_WhatIsGround;                  // A mask determining what is ground to the character

    private Transform m_GroundCheck;    // A position marking where to check if the player is grounded.
    private Transform m_CeilingCheck;   // A position marking where to check for ceilings
    private Transform m_AirCheckSW;
    private Transform m_AirCheckNE;
    private Transform m_AirCheckSE;
    private Transform m_AirCheckNW;
    const float k_GroundedRadius = .2f; // Radius of the overlap circle to determine if grounded
    public bool m_Grounded;            // Whether or not the player is grounded.
    private bool m_SWGrounded;
    private bool m_NEGrounded;
    private bool m_SEGrounded;
    private bool m_NWGrounded;
    private Animator anim;
    const float k_CeilingRadius = .01f; // Radius of the overlap circle to determine if the player can stand up
    private Rigidbody m_Rigidbody;
    private bool m_FacingRight = true;  // For determining which way the player is currently facing.
    public bool isEye = false;
    public bool isHand = false;
    private bool isWalking;
    private bool isSneaking;
    public float colMod = 1f;
    private Vector3 acceleration;
    private Vector3 lastVelocity;

    private void Awake() {
        // Setting up references.
        m_GroundCheck = transform.Find("GroundCheck");
        m_CeilingCheck = transform.Find("CeilingCheck");
        m_Rigidbody = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        anim.SetBool("isRight", true);
        if (isEye) {
            m_AirCheckSW = transform.Find("AirCheckSW");
            m_AirCheckNE = transform.Find("AirCheckNE");
            m_AirCheckSE = transform.Find("AirCheckSE");
            m_AirCheckNW = transform.Find("AirCheckNW");
        }

        BatteryManager.OnBatteryEmpty += EyeForceDrop;
    }

    private void FixedUpdate()
	{
        acceleration = (m_Rigidbody.velocity - lastVelocity) / Time.fixedDeltaTime; lastVelocity = m_Rigidbody.velocity;
        if (!DisguiseManager.primaryManager.GetIsDisguised())
        {
            if (acceleration != new Vector3(0, 0, 0) || m_Rigidbody.velocity != new Vector3(0, 0, 0))
                anim.SetBool("isMoving", true);
            else
                anim.SetBool("isMoving", false);
        }
        m_Grounded  = false;
        m_SWGrounded = false;
        m_NEGrounded = false;
        m_SEGrounded = false;
        m_NWGrounded = false;
        if(isEye)
            m_Rigidbody.useGravity = true;

        // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
        // This can be done using layers instead but Sample Assets will not overwrite your project settings.
        Collider[] colliders = Physics.OverlapSphere(m_GroundCheck.position, k_GroundedRadius/colMod, m_WhatIsGround);
        for (int i = 0; i < colliders.Length; i++) {
            if (colliders[i].gameObject != gameObject && !colliders[i].isTrigger) {
                m_Grounded = true;
                //print(colliders[i].gameObject.name);
                if (isEye)
                    m_Rigidbody.useGravity = false;
            }
        }

        if (isEye) {
            Collider[] swColliders = Physics.OverlapSphere(m_AirCheckSW.position, k_GroundedRadius / colMod, m_WhatIsGround);
            for (int i = 0; i < swColliders.Length; i++) {
                if (swColliders[i].gameObject != gameObject) {
                    m_SWGrounded = true;
                    //print("SW");
                }
            }
            Collider[] neColliders = Physics.OverlapSphere(m_AirCheckNE.position, k_GroundedRadius / colMod, m_WhatIsGround);
            for (int i = 0; i < neColliders.Length; i++) {
                if (neColliders[i].gameObject != gameObject) {
                    m_NEGrounded = true;
                    //print("NE");
                }
            }
            Collider[] seColliders = Physics.OverlapSphere(m_AirCheckSE.position, k_GroundedRadius / colMod, m_WhatIsGround);
            for (int i = 0; i < seColliders.Length; i++) {
                if (seColliders[i].gameObject != gameObject) {
                    m_SEGrounded = true;
                    //print("SE");
                }
            }
            Collider[] nwColliders = Physics.OverlapSphere(m_AirCheckNW.position, k_GroundedRadius / colMod, m_WhatIsGround);
            for (int i = 0; i < nwColliders.Length; i++) {
                if (nwColliders[i].gameObject != gameObject) {
                    m_NWGrounded = true;
                    //print("NW");
                }
            }
        }
    }

    private void EyeForceDrop() {
        Move(0f, -5f, false);
    }

    public void Move(float horMove, float verMove, bool jump) {
        if (!DisguiseManager.primaryManager.GetIsDisguised())
        {
            if (horMove > 0)
                anim.SetBool("isRight", true);
            if (horMove < 0)
                anim.SetBool("isRight", false);
        }

        //only control the player if grounded or airControl is turned on
        if (m_Grounded) {

            if (!isEye && !isHand && Input.GetButtonDown("Walk")) {
                isWalking = !isWalking;
                isSneaking = false;

                if (!DisguiseManager.primaryManager.GetIsDisguised())
                    anim.SetBool("isWalking", isWalking);

                if (isWalking)
                    m_MaxSpeed = m_WalkSpeed;
                else
                    m_MaxSpeed = m_RunSpeed;
            }

            if (!isEye && !isHand && Input.GetButtonDown("Sneak"))
            {
                isSneaking = !isSneaking;
                isWalking = false;

                if (!DisguiseManager.primaryManager.GetIsDisguised())
                    anim.SetBool("isSneaking", isSneaking);

                if (isSneaking)
                    m_MaxSpeed = m_SneakSpeed;
                else
                    m_MaxSpeed = m_RunSpeed;
            }

            // The Speed animator parameter is set to the absolute value of the horizontal input.
            // Move the character
            if (isEye) {
                Vector2 vel = new Vector2(horMove * m_MaxSpeed, verMove * m_MaxSpeed);

                if (m_SWGrounded && m_SEGrounded) {
                    vel = new Vector2(vel.x, -.5f);
                }

                if (m_NWGrounded && m_NEGrounded) {
                    vel = new Vector2(vel.x, verMove * .5f);
                }

                m_Rigidbody.velocity = vel;
                
            }
            else
                m_Rigidbody.velocity = new Vector2(horMove * m_MaxSpeed, m_Rigidbody.velocity.y);
 
        }

        // If the player should jump...
        if (m_Grounded && jump) {
            // Add a vertical force to the player.
            m_Grounded = false;
            m_Rigidbody.AddForce(new Vector2(0f, m_JumpForce));
        }
    }

    void OnDestroy() {
        BatteryManager.OnBatteryEmpty -= EyeForceDrop;
    }*/
}
