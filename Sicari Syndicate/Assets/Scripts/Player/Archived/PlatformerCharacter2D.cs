using System;
using UnityEngine;

/// <summary>
/// Script Template: Unity Technologies
/// Editor: Michael DiLucca
/// As of 10/27/2016 This script will be refactored and duplicated to a 3D version
/// leaving this version as a referance, and if needed, a go back.
/// We plan to switch to a 3D engine to run our 2D game for collision reasons.
/// In order to do this we start by changing the player controller to 3D
/// PLEASE SEE: PlatformerCharacter3D AS THIS SCRIPT IS CURRENTLY OBSOLETE!!!
/// </summary>

public class PlatformerCharacter2D : MonoBehaviour
{
    [SerializeField] private float m_MaxSpeed = 10f;                    // The fastest the player can travel in the x axis.
    [SerializeField] private float m_JumpForce = 400f;                  // Amount of force added when the player jumps.
    [SerializeField] private LayerMask m_WhatIsGround;                  // A mask determining what is ground to the character

    private Transform m_GroundCheck;    // A position marking where to check if the player is grounded.
    const float k_GroundedRadius = .2f; // Radius of the overlap circle to determine if grounded
    private bool m_Grounded;            // Whether or not the player is grounded.
    private Transform m_CeilingCheck;   // A position marking where to check for ceilings
    const float k_CeilingRadius = .01f; // Radius of the overlap circle to determine if the player can stand up
    private Rigidbody2D m_Rigidbody2D;
    private bool m_FacingRight = true;  // For determining which way the player is currently facing.
    public bool isEye = false;

    private void Awake()
    {
        // Setting up references.
        m_GroundCheck = transform.Find("GroundCheck");
        m_CeilingCheck = transform.Find("CeilingCheck");
        m_Rigidbody2D = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        m_Grounded = false;

        // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
        // This can be done using layers instead but Sample Assets will not overwrite your project settings.
        Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject)
                m_Grounded = true;
        }
    }

    public void Move(float horMove, float verMove, bool jump)
    {
        //only control the player if grounded or airControl is turned on
        if (m_Grounded)
        {
            // The Speed animator parameter is set to the absolute value of the horizontal input.
            // Move the character
            if (isEye)
                m_Rigidbody2D.velocity = new Vector2(horMove * m_MaxSpeed, verMove * m_MaxSpeed);
            else
                m_Rigidbody2D.velocity = new Vector2(horMove * m_MaxSpeed, m_Rigidbody2D.velocity.y);
        }

        // If the player should jump...
        if (m_Grounded && jump)
        {
            // Add a vertical force to the player.
            m_Grounded = false;
            m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce));
        }
    }
}
