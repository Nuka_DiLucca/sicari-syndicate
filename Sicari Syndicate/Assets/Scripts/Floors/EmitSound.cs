﻿using UnityEngine;

/// <summary>
/// The character emits a sound based upon the current floor type that alerts guards within range.
/// </summary>
public class EmitSound : MonoBehaviour
{
    #region Variables
    /// <summary>
    /// The player's platformer script.
    /// </summary>
    [HideInInspector] public PlatformerCharacter3DKaida kaida;
    [HideInInspector] public Platformer3DUserControlKaida control;
    /// <summary>
    /// The sphere used to represent the sound emitted.
    /// </summary>
    public Transform sphere;
    /// <summary>
    /// This gameObject's Rigidbody.
    /// </summary>
    [HideInInspector] public Rigidbody rb;

    /// <summary>
    /// The default floor parameters when not standing on a special floor.
    /// </summary>
    [SerializeField] private FloorParams _defaultFloor;
    /// <summary>
    /// The parameters of the floor the player is currently standing on.
    /// </summary>
    [HideInInspector] public FloorParams currentFloor;
    /// <summary>
    /// Is the player moving?
    /// </summary>
    private bool _kaidaIsMoving;
    #endregion

    #region Getters & Setters
    /// <summary>
    /// Returns the default floor parameters.
    /// </summary>
    public FloorParams DefaultFloor { get { return _defaultFloor; } }
    #endregion

    #region Functions

    public delegate void SoundEventHandler(bool moving);
    public static event SoundEventHandler KaidaIsMoving;
    public static void RaiseKaidaIsMoving(bool moving)
    {
        if (KaidaIsMoving != null)
            KaidaIsMoving(moving);
    }

    public void ResetKaidaIsMoving(bool moving)
    {
        _kaidaIsMoving = moving;
        ResizeSphere();
    }
    /// <summary>
    /// Resizes the sound sphere based upon the floor's sound parameters.
    /// </summary>
    public void ResizeSphere()
    {
        if (_kaidaIsMoving && !kaida.IsSneaking)
        {
            if (currentFloor.Sound != null)
            {
                Debug.Log(control);
                Debug.Log(control.movementSound);
                Debug.Log(control.movementSoundPlayer);
                control.movementSound = currentFloor.Sound;
            }
            if (kaida.IsWalking)
            {
				sphere.localScale = new Vector3(currentFloor.WalkDiameter, currentFloor.WalkDiameter, currentFloor.WalkDiameter);
                control.movementSoundPlayer.volume = currentFloor.WalkVolume;
            }
            else
            {
				sphere.localScale = new Vector3(currentFloor.RunDiameter, currentFloor.RunDiameter, currentFloor.RunDiameter);
                control.movementSoundPlayer.volume = currentFloor.RunVolume;
            }
        }
        else
            sphere.localScale = Vector3.zero;
    }
    #endregion

    #region MonoBehaviors
    /// <summary>
    /// Sets the component variables and subscribes functions to events.
    /// </summary>
    private void Awake()
    {
        if (kaida == null)
        {
            kaida = GetComponent<PlatformerCharacter3DKaida>();
            control = GetComponent<Platformer3DUserControlKaida>();
            rb = GetComponent<Rigidbody>();
        }

        KaidaIsMoving += ResetKaidaIsMoving;
    }

    /// <summary>
    /// Sets the current floor sound to the default. 
    /// </summary>
    private void Start()
    {
        currentFloor = _defaultFloor;
    }

    /// <summary>
    /// Unsubscribes functions from events when the object is destroyed.
    /// </summary>
    private void OnDestroy() { KaidaIsMoving -= ResetKaidaIsMoving; }
    #endregion
}