using System;
using UnityEngine;

/// <summary>
/// Script Template: Unity Technologies
/// Editor: Michael DiLucca
/// As of 10/27/2016 This script will be used for collision detection for the player controller
/// Please refer to this script when dealing with any and all player to character interactions
/// on the level of controlls and behaviors.
/// PLEASE NOTE: PlatformerCharacter2D IS CURRENTLY OBSOLETE!!!
/// </summary>

abstract public class PlatformerCharacter3D : MonoBehaviour {
	
    [SerializeField]
	protected float m_MaxSpeed = 3f;                    // The fastest the player can travel in the x axis.
    public float MaxSpeed { get { return m_MaxSpeed; } }
    [SerializeField]
	protected float m_RunSpeed = 3f;                    // The fastest the player can travel in the x axis.
    public float RunSpeed { get { return m_RunSpeed; } }
    private bool isRunning;
    public bool IsRunning { get { return isRunning; } }
    //[SerializeField]
    //private float m_JumpForce = 400f;               	// Amount of force added when the player jumps.
    [SerializeField]
	protected LayerMask m_WhatIsGround;                 	// A mask determining what is ground to the character

    private Transform m_GroundCheck;    				// A position marking where to check if the player is grounded.
    private Transform m_CeilingCheck;   				// A position marking where to check for ceilings

	protected const float k_GroundedRadius = .2f; 				// Radius of the overlap circle to determine if grounded
    public bool m_Grounded;            					// Whether or not the player is grounded.

	protected Animator anim;
    const float k_CeilingRadius = .01f; 				// Radius of the overlap circle to determine if the player can stand up
	protected Rigidbody m_Rigidbody;
    private bool m_FacingRight = true;  				// For determining which way the player is currently facing.
	public float colMod = 1f;

	protected Vector3 acceleration;
	protected Vector3 lastVelocity;

	protected virtual void Awake() {
        // Setting up references.
        m_GroundCheck = transform.Find("GroundCheck");
        m_CeilingCheck = transform.Find("CeilingCheck");
        m_Rigidbody = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        anim.SetBool("isRight", true);
    }

    protected virtual void FixedUpdate() {
		
        acceleration = (m_Rigidbody.velocity - lastVelocity) / Time.fixedDeltaTime;
        lastVelocity = m_Rigidbody.velocity;

		m_Grounded  = false;

        // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
        // This can be done using layers instead but Sample Assets will not overwrite your project settings.
        Collider[] colliders = Physics.OverlapSphere(m_GroundCheck.position, k_GroundedRadius/colMod, m_WhatIsGround);
        for (int i = 0; i < colliders.Length; i++) {
            if (colliders[i].gameObject != gameObject && !colliders[i].isTrigger) {
                m_Grounded = true;
            }
        }
    }

    public virtual void Move(float horMove, float verMove, bool jump) {

        //only control the player if grounded or airControl is turned on
        if (m_Grounded) {
			
        	m_Rigidbody.velocity = new Vector2(horMove * m_MaxSpeed, m_Rigidbody.velocity.y);
        }

        /*// If the player should jump...
        if (m_Grounded && jump) {
            // Add a vertical force to the player.
            m_Grounded = false;
            m_Rigidbody.AddForce(new Vector2(0f, m_JumpForce));
        }*/
    }
}
