﻿using UnityEngine;
using System;
using System.Collections;

public class LimbManager : MonoBehaviour
{
    private GameObject player;

    PlatformerCharacter3D handCharacter;
    PlatformerCharacter3D eyeCharacter;

    private bool canReturnHand;
    private bool canReturnEye;

    public GameObject hand;
    public GameObject eye;
    public SmoothFollowCSharp camera;
    public KeyCode eyeKey;
    public KeyCode handKey;

    public delegate void LimbEventHandler();
    public static event LimbEventHandler OnEyePickup;
    public static event LimbEventHandler OnHandPickup;

    public static void RaiseOnEyePickup() {
        if (OnEyePickup != null)
            OnEyePickup();
    }

    public static void RaiseOnHandPickup() {
        if (OnHandPickup != null)
            OnHandPickup();
    }

    // Use this for initialization
    void Start() {

        // Get the main character in constant time
        player = GameObject.FindWithTag("Player");
        camera = Camera.main.gameObject.GetComponent<SmoothFollowCSharp>();
       
		if (hand != null)
        	handCharacter = hand.GetComponent<PlatformerCharacter3D>();
		if (eye != null)
        	eyeCharacter = eye.GetComponent<PlatformerCharacter3D>();
    }

    // Update is called once per frame
    void Update()
	{

		if (Input.GetKeyDown(handKey) && canReturnHand)
		{
			HandleHandPickup();
        }

        if (Input.GetKeyDown(eyeKey) && canReturnEye)
		{
			HandleEyePickup();
        }
    }

	public void HandleHandPickup()
	{
		hand.SetActive(false);
		canReturnHand = false;
		camera.target = player;
		PlayerManager.playerCharacter.enabled = true;
		PlayerManager.playerController.enabled = true;

		RaiseOnHandPickup();
	}

	public void HandleEyePickup()
	{
		eye.SetActive(false);
		canReturnEye = false;
		camera.target = player;
		PlayerManager.playerCharacter.enabled = true;
		PlayerManager.playerController.enabled = true;

		RaiseOnEyePickup();
	}

    void OnTriggerEnter(Collider other) {
        if (other.name.ToLower() == "hand")
            canReturnHand = true;
        if (other.name.ToLower() == "eye")
            canReturnEye = true;
    }

    void OnTriggerExit(Collider other)
	{
        if (other.name.ToLower() == "hand")
            canReturnHand = false;
        if (other.name.ToLower() == "eye")
            canReturnEye = false;
    }
}
