﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class TemporaryGoalTarget : MonoBehaviour
{
	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "Player" && EnemyObserver.kaida != null)
		{
			StartCoroutine(DoWin());
		}
	}

	private IEnumerator DoWin()
	{
		Debug.Log("Won.");

		if (EnemyObserver.kaida.win != null)
			EnemyObserver.kaida.win.enabled = true;
		else
			Debug.Log("No win image found in stored Platform3DUserControl.");
		
		EnemyObserver.kaida.enabled = false;

		Time.timeScale = 0f;
		float targetTime = Time.realtimeSinceStartup + 4f;

		while (Time.realtimeSinceStartup < targetTime)
		{
			yield return 0;
		}

		Time.timeScale = 1f;

		foreach (EnemyManager enemy in EnemyObserver.enemies)
			enemy.Unsubscribe();
		EnemyObserver.enemies.Clear();

		SceneManager.LoadScene(SceneManager.GetActiveScene().name);

		if (EnemyObserver.kaida.win != null)
			EnemyObserver.kaida.win.enabled = false;
		else
			Debug.Log("No win image found in stored Platform3DUserControl.");
	}

	public void LoadScene(int scene){
		SceneManager.LoadScene(scene);
	}
}
