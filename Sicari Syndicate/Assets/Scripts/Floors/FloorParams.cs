﻿using System;
using UnityEngine;

/// <summary>
/// Parameters for special types of floors that emit sound when stepped on.
/// </summary>
[Serializable]
public class FloorParams
{
    #region Variables
    /// <summary>
    /// The name of the floor.
    /// </summary>
    [SerializeField] protected string _name = "Silent";
    /// <summary>
    /// The sound the player's footsteps emit when walking.
    /// </summary>
    [SerializeField] protected AudioClip _sound;
    /// <summary>
    /// The volume of the footsteps when walking.
    /// </summary>
    [SerializeField] protected float _walkVolume = 0.25f;
    /// <summary>
    /// The volume of the footsteps when running.
    /// </summary>
    [SerializeField] protected float _runVolume = 0.5f;
    /// <summary>
    /// The diameter of the sound's sphere emitted while walking.
    /// </summary>
    [SerializeField] protected float _walkDiameter = 0;
    /// <summary>
    /// The diameter of the sound's sphere emitted while running.
    /// </summary>
    [SerializeField] protected float _runDiameter = 0;
    #endregion

    #region Getters & Setters
    /// <summary>
    /// Returns the name of the floor.
    /// </summary>
    public string Name { get { return _name; } }
    /// <summary>
    /// Returns the sound footsteps make on the floor.
    /// </summary>
    public AudioClip Sound { get { return _sound; } }
    /// <summary>
    /// Returns the volume of the footsteps while walking.
    /// </summary>
    public float WalkVolume { get { return _walkVolume; } }
    /// <summary>
    /// Returns the volume of the footsteps while running.
    /// </summary>
    public float RunVolume { get { return _runVolume; } }
    /// <summary>
    /// Returns the sound sphere's diameter emitted while walking.
    /// </summary>
    public float WalkDiameter { get { return _walkDiameter; } }
    /// <summary>
    /// Returns the sound sphere's diameter emitted while running.
    /// </summary>
    public float RunDiameter { get { return _runDiameter; } }
    #endregion

    #region Constructors
    /// <summary>
    /// Default Constructor
    /// </summary>
    public FloorParams() { }
    /// <summary>
    /// Copy Constructor
    /// </summary>
    /// <param name="f"></param>
    public FloorParams(FloorParams f)
    {
        _name = f._name;
        _sound = f._sound;
        _walkVolume = f._walkVolume;
        _runVolume = f._runVolume;
        _walkDiameter = f._walkDiameter;
        _runDiameter = f._runDiameter;
    }
    /// <summary>
    /// Custom Constructor
    /// </summary>
    /// <param name="name"></param>
    /// <param name="walkDiameter"></param>
    /// <param name="runDiameter"></param>
    public FloorParams(string name, AudioClip sound, float walkVolume, float runVolume, float walkDiameter, float runDiameter)
    {
        _name = name;
        _sound = sound;
        _walkVolume = walkVolume;
        _runVolume = runVolume;
        _walkDiameter = walkDiameter;
        _runDiameter = runDiameter;
    }
    #endregion
}