﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class TemporaryLevelChangeZone : MonoBehaviour {

	public int sceneToLoad;

	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "Player")
		{
			SceneManager.LoadScene(sceneToLoad);
		}
	}
}
