﻿using UnityEngine;
using System.Collections;

public class SoundSphere : MonoBehaviour
{
	SphereCollider myCol;

	private void Awake()
	{
		myCol = GetComponent<SphereCollider>();
	}

	private void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "Enemy")
		{
			EnemyManager enemyScript = col.GetComponent<EnemyManager>();

			//Debug.Log("Heard ya.");

			if (enemyScript != null && !enemyScript.IsStunned && myCol != null)
			{
				SearchAIPackage newPackage = enemyScript.gameObject.AddComponent<SearchAIPackage>();
				newPackage.initialTarget = transform;
				newPackage.maxSearchDistance = (myCol.radius * (3f / 4f));
				//newPackage.AttachManager(enemyScript);
				//newPackage.AttachAgent();

				//Debug.Log("Forcing SearchAIPackage.");

				enemyScript.ForceNewPackage(newPackage);
			}
		}
	}
}