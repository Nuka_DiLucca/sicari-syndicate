using System;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

/// <summary>
/// Script Template: Unity Technologies
/// Editor: Michael DiLucca
/// As of 10/27/2016 This script will be used for collision detection for the player controller
/// Please refer to this script when dealing with any and all player to character interactions
/// on the level of controlls and behaviors.
/// PLEASE NOTE: Platformer2DUserControl IS CURRENTLY OBSOLETE!!!
/// </summary>

[RequireComponent(typeof(PlatformerCharacter3D))]
public class Platformer3DUserControlHand : Platformer3DUserControl
{
    /// <summary>
    /// The SphereCollider used as a trigger.
    /// </summary>
    [SerializeField] private SphereCollider _soundSphere;

    /// <summary>
    /// The small window of time the trigger will be active.
    /// </summary>
    [SerializeField] private float _emitTime = 0.1f;

    /// <summary>
    /// Used to time the trigger's activation.
    /// </summary>
    private float _timer = 0;

	public override void Start() {

        //		if (myCamera == null) {
        //			if (isMainControl)
        //				myCamera = PlayerManager.handCamera;
        //		}

        if (_soundSphere != null)
            _soundSphere.enabled = false;

		base.Start ();
    }

	protected override void Update() {
        
		base.Update ();

        if (Input.GetButtonDown("Jump"))
        {
            _soundSphere.enabled = true;
            _timer = 0;
        }
        else if (_soundSphere.enabled == true)
        {
            if (_timer >= _emitTime)
            {
                _soundSphere.enabled = false;
                _timer = 0;
            }
            else
                _timer += Time.deltaTime;
        }

        /* // VERY temporary stun function until this all gets reimplemented on proper character controllers.
        if (Input.GetButtonDown("Jump"))
		{
            Collider[] hits = Physics.OverlapSphere(transform.position, 0.3f);

            for (int i = 0; i < hits.Length; i++)
			{
                EnemyManager enemyScript = hits[i].gameObject.GetComponent<EnemyManager>();

                if (enemyScript != null)
				{
					if (!enemyScript.IsStunned)
					{
						if (BatteryManager.primaryBatteryManager != null && BatteryManager.primaryBatteryManager.currentBattery > 0f)
						{
							if (stunBatteryUsage < 0f)
								stunBatteryUsage = 0f;

							BatteryManager.primaryBatteryManager.UseBatteryImmediate(stunBatteryUsage);

                            PlayActionSound(stunSound);

                            enemyScript.StunSelf();
                            break;
                        }
                    }
                }
            }
        }*/
    }
}

