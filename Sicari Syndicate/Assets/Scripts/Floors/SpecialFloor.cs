﻿using UnityEngine;

/// <summary>
/// Contains parameters for the kind of floor the player is walking on.
/// </summary>
public class SpecialFloor : MonoBehaviour
{
    #region Variables
    /// <summary>
    /// The parameters of this floor.
    /// </summary>
    [SerializeField] private FloorParams _floorParams;
    #endregion

    #region Getters & Setters
    /// <summary>
    /// Returns the parameters of this floor.
    /// </summary>
    public FloorParams FloorParams { get { return _floorParams; } }
    #endregion

    #region MonoBehaviors
    /// <summary>
    /// Changes the player's current floor to this one when stepped on.
    /// </summary>
    /// <param name="col"></param>
    private void OnTriggerEnter(Collider col)
    {
        EmitSound e = col.GetComponent<EmitSound>();
        if (e != null)
        {
            e.currentFloor = _floorParams;
            e.ResizeSphere();
        }
    }
    /// <summary>
    /// Changes the player's current floor to the default one when stepped off of.
    /// </summary>
    /// <param name="col"></param>
    private void OnTriggerExit(Collider col)
    {
        EmitSound e = col.GetComponent<EmitSound>();
        if (e != null)
        {
            e.currentFloor = e.DefaultFloor;
            e.ResizeSphere();
        }
    }
    #endregion
}
