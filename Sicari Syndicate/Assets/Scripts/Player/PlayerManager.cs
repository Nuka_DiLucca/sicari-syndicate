﻿using UnityEngine;
using System;
using System.Collections;


/// <summary>
/// This Script manages which character the player is controlling.
/// Player and Limbs should not be confused with Characters
/// Characters includes any and all sprites the player can assume control over
/// using the default controls for movement.
/// </summary>
public class PlayerManager : MonoBehaviour {

	//public static PlayerManager primaryPlayerManager;

	[SerializeField]
    private GameObject player;
	[SerializeField]
    private GameObject hand;
	[SerializeField]
    private GameObject eye;

    public float droneActiveBatteryUsage = 10f;
    public float droneInactiveBatteryUsage = 2.5f;
	public AudioClip droneDedSound;
    public KeyCode handKey;
    public KeyCode eyeKey;

    public static PlatformerCharacter3D playerCharacter;
	public static PlatformerCharacter3D handCharacter;
	public static PlatformerCharacter3D eyeCharacter;
    public static Platformer3DUserControl playerController;
	public static Platformer3DUserControl handController;
	public static Platformer3DUserControl eyeController;
	public static Platformer3DUserControl currentController;
	public static Animator pAnim;
	public static Animator hAnim;
	public static Animator eAnim;
//	public static GameObject playerCamera;
//	public static GameObject handCamera;
//	public static GameObject eyeCamera;
	public static SmoothFollowCSharp cameraFollower;
    Rigidbody playerRigidbody;
    Rigidbody handRigidbody;
    Rigidbody eyeRigidbody;
    AudioListener playerListener;
    AudioListener handListener;
	AudioListener eyeListener;
    private bool canSwitchToEye = true;
    private bool canSwitchToHand = true;
	private bool canPickUpEye = true;
	private bool canPickUpHand = true;
    private bool eyeRecalledThisFrame = false;
    private bool handRecalledThisFrame = false;

    private IEnumerator eyeBatteryRoutine;
    private IEnumerator handBatteryRoutine;

	public delegate void PlayerEventHandler(Transform targetLoc);
	public static event PlayerEventHandler OnEnterOrExit;

	public static void RaiseOnEnterOrExit(Transform targetLoc)
	{
		if (OnEnterOrExit != null)
			OnEnterOrExit(targetLoc);
	}

    void Awake()
	{
		//if (primaryPlayerManager == null)
		//	primaryPlayerManager = this;

		// Get the main character in constant time
		player = GameObject.FindWithTag("Player");
		hand = GameObject.FindWithTag("Hand");
		eye = GameObject.FindWithTag("Eye");

		// Get each characters controller in constant time
		playerCharacter = player.GetComponent<PlatformerCharacter3D>();
		handCharacter = hand.GetComponent<PlatformerCharacter3D>();
		eyeCharacter = eye.GetComponent<PlatformerCharacter3D>();

		// Get each characters controller in constant time
		pAnim = player.GetComponent<Animator>();
		hAnim = hand.GetComponent<Animator>();
		eAnim = eye.GetComponent<Animator>();

		playerController = player.GetComponent<Platformer3DUserControl>();
		handController = hand.GetComponent<Platformer3DUserControl>();
		eyeController = eye.GetComponent<Platformer3DUserControl>();

		currentController = playerController;

		// Get each characters' camera in constant time.
//		playerCamera = player.transform.GetChild(0).gameObject;
//		handCamera = hand.transform.GetChild(0).gameObject;
//		eyeCamera = eye.transform.GetChild(0).gameObject;
		cameraFollower = Camera.main.GetComponent<SmoothFollowCSharp>();

		// Get each characters' Rigidbody in constant time.
		playerRigidbody = player.GetComponent<Rigidbody>();
		handRigidbody = hand.GetComponent<Rigidbody>();
		eyeRigidbody = eye.GetComponent<Rigidbody>();

        // Get each characters' Audio Listener in constant time.
        playerListener = player.GetComponent<AudioListener>();
        handListener = hand.GetComponent<AudioListener>();
        eyeListener = eye.GetComponent<AudioListener>();
		handListener = hand.GetComponent<AudioListener>();
		eyeListener = eye.GetComponent<AudioListener>();
        
        Subscribe();
    }

    // Use this for initialization
    void Start() {
        hand.SetActive(false);
        eye.SetActive(false);
		cameraFollower.target = player;
    }

    private void Subscribe() {

        LimbManager.OnEyePickup += ReturnEye;
        LimbManager.OnHandPickup += ReturnHand;
        BatteryManager.OnBatteryEmpty += DroneDed;
    }

    // Update is called once per frame
    // TODO: We can take out all copies of camera and make a single instance.
    //      With this we can just lerp the camera to the different characters
    // when a button is pressed. 
    void Update() {

        if (Input.GetKeyDown(handKey) && !handRecalledThisFrame) { 
            // If the hand is on the map and the player wants to switch to it.
			if (canSwitchToHand && (playerController.enabled && cameraFollower.target == player ||
				eyeController.enabled && cameraFollower.target == eye) && 
                BatteryManager.primaryBatteryManager != null && BatteryManager.primaryBatteryManager.currentBattery > 0f) {
                if (!hand.activeSelf) {
                    hand.SetActive(true);
                    hand.transform.position = player.transform.position;
                }

                playerCharacter.enabled = false;
                handCharacter.enabled = true;
                eyeCharacter.enabled = false;

				pAnim.SetBool("isMoving", false);
				hAnim.SetBool("isMoving", false);
				eAnim.SetBool ("isMoving", false);

                playerController.enabled = false;
                handController.enabled = true;
                eyeController.enabled = false;

                playerListener.enabled = false;
                eyeListener.enabled = false;
                handListener.enabled = true;

                currentController = handController;

//                playerCamera.SetActive(false);
//                handCamera.SetActive(true);
//                eyeCamera.SetActive(false);
				cameraFollower.target = hand;

                playerRigidbody.velocity = Vector3.zero;
                handRigidbody.velocity = Vector3.zero;
                eyeRigidbody.velocity = Vector3.zero;

                HandleDroneBattery(ref handBatteryRoutine, droneActiveBatteryUsage);

                if (eye.activeSelf) {
                    HandleDroneBattery(ref eyeBatteryRoutine, droneInactiveBatteryUsage);
                }

				canPickUpHand = true;

// IF Hand -> Player
			} else if (canPickUpHand && handController.enabled && cameraFollower.target == hand) {

                playerCharacter.enabled = true;
                handCharacter.enabled = false;
                eyeCharacter.enabled = false;

                playerController.enabled = true;
                handController.enabled = false;
                eyeController.enabled = false;

				pAnim.SetBool("isMoving", false);
				hAnim.SetBool("isMoving", false);
				eAnim.SetBool ("isMoving", false);

                handListener.enabled = false;
                eyeListener.enabled = false;
                playerListener.enabled = true;

                currentController = playerController;

//                playerCamera.SetActive(true);
//                handCamera.SetActive(false);
//                eyeCamera.SetActive(false);
				cameraFollower.target = player;


                playerRigidbody.velocity = Vector3.zero;
                handRigidbody.velocity = Vector3.zero;
                eyeRigidbody.velocity = Vector3.zero;

                HandleDroneBattery(ref handBatteryRoutine, droneInactiveBatteryUsage);

				canPickUpHand = false;
            }
        }
			
// IF Player -> Eye
        if (Input.GetKeyDown(eyeKey) && !eyeRecalledThisFrame) {

            // If the eye is on the map and the player wants to switch to it.
			if (canSwitchToEye && (playerController.enabled && cameraFollower.target == player ||
				handController.enabled && cameraFollower.target == hand) &&
                BatteryManager.primaryBatteryManager != null && BatteryManager.primaryBatteryManager.currentBattery > 0f) {

                if (!eye.activeSelf) {
                    eye.SetActive(true);
                    eye.transform.position = player.transform.position;
                }

                playerCharacter.enabled = false;
                handCharacter.enabled = false;
                eyeCharacter.enabled = true;

				pAnim.SetBool("isMoving", false);
				hAnim.SetBool("isMoving", false);
				eAnim.SetBool ("isMoving", false);

                playerController.enabled = false;
                handController.enabled = false;
                eyeController.enabled = true;

                playerListener.enabled = false;
                handListener.enabled = false;
                eyeListener.enabled = true;

				currentController = eyeController;

//                playerCamera.SetActive(false);
//                handCamera.SetActive(false);
//                eyeCamera.SetActive(true);
				cameraFollower.target = eye;

                playerRigidbody.velocity = Vector3.zero;
                handRigidbody.velocity = Vector3.zero;
                eyeRigidbody.velocity = Vector3.zero;

                HandleDroneBattery(ref eyeBatteryRoutine, droneActiveBatteryUsage);

                if (hand.activeSelf) {
                    HandleDroneBattery(ref handBatteryRoutine, droneInactiveBatteryUsage);
                }

				canPickUpEye = true;

// IF Eye -> Player
			} else if (canPickUpEye && eyeController.enabled && cameraFollower.target == eye) {

                playerCharacter.enabled = true;
                handCharacter.enabled = false;
                eyeCharacter.enabled = false;

                playerController.enabled = true;
                handController.enabled = false;
                eyeController.enabled = false;

				pAnim.SetBool("isMoving", false);
				hAnim.SetBool("isMoving", false);
				eAnim.SetBool ("isMoving", false);

                handListener.enabled = false;
                eyeListener.enabled = false;
                playerListener.enabled = true;

                currentController = playerController;

//                playerCamera.SetActive(true);
//                handCamera.SetActive(false);
//                eyeCamera.SetActive(false);
				cameraFollower.target = player;

                playerRigidbody.velocity = Vector3.zero;
                handRigidbody.velocity = Vector3.zero;
                eyeRigidbody.velocity = Vector3.zero;

                HandleDroneBattery(ref eyeBatteryRoutine, droneInactiveBatteryUsage);

				canPickUpEye = false;
            }
        }

        handRecalledThisFrame = false;
        eyeRecalledThisFrame = false;
    }

    private void HandleDroneBattery(ref IEnumerator coroutine, float usageAmount) {

        if (BatteryManager.primaryBatteryManager != null) {
            if (usageAmount < 0f)
                usageAmount = 0f;

            if (coroutine != null) {
                BatteryManager.primaryBatteryManager.EndBatteryUsage(coroutine);
                StopCoroutine(coroutine);
            }

            coroutine = BatteryManager.primaryBatteryManager.UseBattery(usageAmount);
            StartCoroutine(coroutine);
        }
    }

    public void ReturnHand() {
        //hand.SetActive(false);
        //hand.transform.position = player.transform.position;

        if (BatteryManager.primaryBatteryManager != null && handBatteryRoutine != null) {
            if (canSwitchToHand)
                BatteryManager.primaryBatteryManager.EndBatteryUsage(handBatteryRoutine);

            StopCoroutine(handBatteryRoutine);
        }

        canSwitchToHand = true;
        handRecalledThisFrame = true;
    }

    public void ReturnEye() {
        //eye.SetActive(false);
        //eye.transform.position = player.transform.position;

        if (BatteryManager.primaryBatteryManager != null && eyeBatteryRoutine != null) {
            if (canSwitchToEye)
                BatteryManager.primaryBatteryManager.EndBatteryUsage(eyeBatteryRoutine);

            StopCoroutine(eyeBatteryRoutine);
        }

        canSwitchToEye = true;
        eyeRecalledThisFrame = true;
    }

    private void DroneDed() {

		bool playSound = false;

        if (eye.activeSelf) {
            canSwitchToEye = false;
			playSound = true;

            if (BatteryManager.primaryBatteryManager != null && eyeBatteryRoutine != null) {
                BatteryManager.primaryBatteryManager.EndBatteryUsage(eyeBatteryRoutine);
                StopCoroutine(eyeBatteryRoutine);
            }
        }

        if (hand.activeSelf) {
            canSwitchToHand = false;
			playSound = true;

            if (BatteryManager.primaryBatteryManager != null && handBatteryRoutine != null) {
                BatteryManager.primaryBatteryManager.EndBatteryUsage(handBatteryRoutine);
                StopCoroutine(handBatteryRoutine);
            }
        }

        playerCharacter.enabled = true;
        handCharacter.enabled = false;
        eyeCharacter.enabled = false;

		pAnim.SetBool("isMoving", false);
		hAnim.SetBool("isMoving", false);
		eAnim.SetBool ("isMoving", false);

        playerController.enabled = true;
        handController.enabled = false;
        eyeController.enabled = false;

        handListener.enabled = false;
        eyeListener.enabled = false;
        playerListener.enabled = true;

        currentController = playerController;

//        playerCamera.SetActive(true);
//        handCamera.SetActive(false);
//        eyeCamera.SetActive(false);
		cameraFollower.target = player;

		if (playSound && currentController != null)
			currentController.PlayActionSound(droneDedSound);

        playerRigidbody.velocity = Vector3.zero;
        handRigidbody.velocity = Vector3.zero;
        eyeRigidbody.velocity = Vector3.zero;
    }

    void OnDestroy()
	{
		//if (primaryPlayerManager == this)
		//	primaryPlayerManager = null;

        Unsubscribe();
    }

    private void Unsubscribe() {

        LimbManager.OnEyePickup -= ReturnEye;
        LimbManager.OnHandPickup -= ReturnHand;
        BatteryManager.OnBatteryEmpty -= DroneDed;
    }
}
