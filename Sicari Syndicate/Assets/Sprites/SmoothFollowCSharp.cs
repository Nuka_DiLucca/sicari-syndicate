﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Camera-Control/Smooth Folow CSharp")]

public class SmoothFollowCSharp : MonoBehaviour {

	// Target to follow
	public GameObject target;
	// Distance in the x-y plane to the target
	public float distance = 10f;
	// Dampening factor
	public float dampening = 2f;
	public float ymod = 2f;
	public float xmod = 0f;

	// Update is called once per frame
	void LateUpdate () {

		// If no target return
		if (!target)
			return;
		
		Vector3 currentPosition = transform.position;
		Vector3 wantedPosition = new Vector3 (target.transform.position.x + xmod,
			target.transform.position.y + ymod,
			distance);

		transform.position = new Vector3 ((Mathf.Lerp (currentPosition.x, wantedPosition.x, dampening * Time.deltaTime)),
			(Mathf.Lerp (currentPosition.y, wantedPosition.y, dampening * Time.deltaTime)),
			distance);
	}
}
