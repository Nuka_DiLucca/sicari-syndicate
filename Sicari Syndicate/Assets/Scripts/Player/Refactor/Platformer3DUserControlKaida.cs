using System;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

/// <summary>
/// Script Template: Unity Technologies
/// Editor: Michael DiLucca
/// As of 10/27/2016 This script will be used for collision detection for the player controller
/// Please refer to this script when dealing with any and all player to character interactions
/// on the level of controlls and behaviors.
/// PLEASE NOTE: Platformer2DUserControl IS CURRENTLY OBSOLETE!!!
/// </summary>

[RequireComponent(typeof(PlatformerCharacter3D))]
public class Platformer3DUserControlKaida : Platformer3DUserControl
{

    private guardAnimation guardAnimControl;
	public Image win;							// Temporary.
	public Image loss;							// Temporary.
	private bool channelingRecall = false;
    private Animator myAnim;
	private float startTime, currTime;
	public GameObject recallCircle;

    protected override void Awake() {

		isMainControl = true;
		EnemyObserver.kaida = this;
        myAnim = GetComponent<Animator>();

		base.Awake ();

        BatteryManager.OnBatteryEmpty += DeactivateGuardControls;
        guardAnimControl = gameObject.GetComponent<guardAnimation>();
    }

	public override void Start() {
		
        if (DisguiseManager.primaryManager != null)
            disguiseManager = DisguiseManager.primaryManager;
        else
            disguiseManager = gameObject.GetComponent<DisguiseManager>();

		if ((win == null || loss == null) && HUDManager.primaryHUDManager != null)
		{
			win = HUDManager.primaryHUDManager.winImage;
			loss = HUDManager.primaryHUDManager.lossImage;
		}

		base.Start ();
    }

	protected override void Update() {

		base.Update ();

		if (Input.GetKeyDown ("g")) {
			currTime = Time.time;
			channelingRecall = true;
		}
		if (Input.GetKey ("g")) {
			startTime = Time.time;
			float diffTime = startTime - currTime;
			recallCircle.transform.localScale = new Vector3 (diffTime,diffTime,diffTime);
		}
		if (Input.GetKeyUp ("g")) {
			recallCircle.transform.localScale = Vector3.zero;
			channelingRecall = false;
		}

        // VERY temporary stun function until this all gets reimplemented on proper character controllers.
        if (Input.GetButtonDown("Jump"))
		{
            Collider[] hits = Physics.OverlapSphere(transform.position, 0.3f);

            for (int i = 0; i < hits.Length; i++)
			{
                EnemyManager enemyScript = hits[i].gameObject.GetComponent<EnemyManager>();

                if (enemyScript != null)
				{
					if (!enemyScript.IsStunned)
					{
						if (BatteryManager.primaryBatteryManager != null && BatteryManager.primaryBatteryManager.currentBattery > 0f)
						{
							if (stunBatteryUsage < 0f)
								stunBatteryUsage = 0f;

							BatteryManager.primaryBatteryManager.UseBatteryImmediate(stunBatteryUsage);
                           
                            PlayActionSound(stunSound);
                            if(myAnim != null)
                            {
                                myAnim.SetTrigger("Stun");
                            }

                            enemyScript.StunSelf();
                            break;
                        }
                    }
					else if (enemyScript.IsStunned && disguiseManager != null)
					{
                        SpriteRenderer newRend = enemyScript.gameObject.GetComponent<SpriteRenderer>();
                        Animator newAnim = enemyScript.gameObject.GetComponent<Animator>();

                        if (newRend != null && newAnim != null)
						{
                            //Disguise newDisguise = new Disguise(newRend.sprite, newAnim.runtimeAnimatorController, enemyScript.tagsInDisguise);
                            Disguise newDisguise = new Disguise(enemyScript.disguiseSprite, newAnim.runtimeAnimatorController, enemyScript.tagsInDisguise);
                            if (disguiseManager.AddDisguise(newDisguise))
								PlayActionSound(scanSound);
                        }
                    }
                }
				HackableObject hackObj = hits [i].gameObject.GetComponent<HackableObject> ();

				if (hackObj != null) {
					hackObj.Hack ();
				}
            }
        }

		if (disguiseManager != null && isMainControl) {
            if (Input.GetKeyDown(KeyCode.Alpha1)) {
                if (disguiseManager.SwapToDisguise(0)) {

					//actionSoundPlayer.pitch = Random.range (-.5, 1.5);
                    PlayActionSound(swapToDisguise);
                    ActivateGuardControls();
                }
            } else if (Input.GetKeyDown(KeyCode.Alpha2)) {
                if (disguiseManager.SwapToDisguise(1)) {
					
					//actionSoundPlayer.pitch = Random.range (-.5, 1.5);
                    PlayActionSound(swapToDisguise);
                    ActivateGuardControls();
                }
            } else if (Input.GetKeyDown(KeyCode.Alpha3)) {
                if (disguiseManager.SwapToDisguise(3)) {

					//actionSoundPlayer.pitch = Random.range (-.5, 1.5);
                    PlayActionSound(swapToDisguise);
                    ActivateGuardControls();
                }
            } else if (Input.GetKeyDown(KeyCode.R)) {
                if (disguiseManager.EndDisguise()) {
					
					//actionSoundPlayer.pitch = Random.range (-.5, 1.5);
					PlayActionSound(endDisguise);
                    DeactivateGuardControls();
                }
            }
        }
    }

	protected override void FixedUpdate() {
		if (!channelingRecall) {
			base.FixedUpdate ();
		} else {
			transform.GetComponent<Rigidbody>().velocity = Vector3.zero;
		}
	}

    /// <summary>
    /// Activate the guard control/animation scheme.
    /// </summary>
    private void ActivateGuardControls() {
		
        if (guardAnimControl != null) 
            guardAnimControl.enabled = true;
    }

    /// <summary>
    /// Deactivate the guard control/animation scheme.
    /// </summary>
    private void DeactivateGuardControls() {
        if (guardAnimControl != null) {
			
            guardAnimControl.enabled = false;
            if (myRenderer != null)
                myRenderer.flipX = false;
        }
    }

    void OnDestroy() {
        BatteryManager.OnBatteryEmpty -= DeactivateGuardControls;
    }
}

