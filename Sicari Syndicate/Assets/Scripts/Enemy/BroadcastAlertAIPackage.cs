﻿using UnityEngine;
using System.Collections;

public class BroadcastAlertAIPackage : AIPackage
{
	public override void StartPackage()
	{
		EnemyObserver.RaiseAlertAllOtherEnemies(manager);

		EndPackage();
	}

	public override void LogInfo()
	{
		Debug.Log("LogInfo: This is a Broadcast Alert AI Package.");
	}
}
