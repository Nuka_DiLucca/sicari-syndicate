﻿using System.Collections;
using UnityEngine;

public class SurvCam : HackableObject
{
    #region Variables
	public GameObject scanArea;
    #endregion

    #region Functions
    //private IEnumerator Open()
    //{
		//scanArea.SetActive (false);
		//yield return new WaitForSeconds(5);
		//scanArea.SetActive (true);
    //}
    #endregion 

	public override void Hack()
	{
		sound.Play ();
		scanArea.SetActive (false);

		//StartCoroutine(Open());
	}
}