﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BatteryManager : MonoBehaviour
{
	public static BatteryManager primaryBatteryManager;

	public float currentBattery = 100f;
	public float maxBattery = 100f;
	public Text currentBatteryField;
	public Text maxBatteryField;
	public RectTransform batteryBarContainer;
	public RectTransform batteryBar;
    public Color negativeBatteryColor;
	public float timeUntilRecharge = 2.5f;
	public float rechargedPerSecond = 10f;

	private int numUsers;
	private IEnumerator rechargeCoroutine;
    private Image batteryBG;
    private Color baseColor;

	public delegate void BatteryEventHandler();
	public static event BatteryEventHandler OnBatteryEmpty;

	public static void RaiseOnBatteryEmpty()
	{
		if (OnBatteryEmpty != null)
			OnBatteryEmpty();
	}

	void Awake()
	{
		if (primaryBatteryManager == null)
			primaryBatteryManager = this;

		rechargeCoroutine = RechargeBattery();

        if (batteryBarContainer != null)
        {
            batteryBG = batteryBarContainer.GetComponent<Image>();

            if (batteryBG != null)
                baseColor = batteryBG.color;
        }
	}

	public bool UseBatteryImmediate(float batteryToUse)
	{
		if (currentBattery > 0f)
		{
			StopCoroutine(rechargeCoroutine);

			currentBattery -= batteryToUse;

			UpdateCurrentBatteryHUD();

			if (currentBattery <= 0f)
				RaiseOnBatteryEmpty();

			EndBatteryUsageImmediate();

			return true;
		}

		return false;
	}

	public IEnumerator UseBattery(float usedPerSecond)
	{
		if (usedPerSecond >= 0f && currentBattery > 0f)
		{
			numUsers++;

			StopCoroutine(rechargeCoroutine);

			while (currentBattery > 0f && usedPerSecond > 0f)
			{
				yield return new WaitForEndOfFrame();

				currentBattery -= Time.deltaTime * usedPerSecond;

				if (currentBattery < 0f)
					currentBattery = 0f;

				UpdateCurrentBatteryHUD();
			}

			if (currentBattery <= 0f)
				RaiseOnBatteryEmpty();
		}
	}

	public void EndBatteryUsage(IEnumerator batteryRoutine)
	{
		if (batteryRoutine != null)
		{
			numUsers--;
			//StopCoroutine(batteryRoutine);

			if (numUsers <= 0)
			{
				numUsers = 0;

				StopCoroutine(rechargeCoroutine);
				rechargeCoroutine = RechargeBattery();
				StartCoroutine(rechargeCoroutine);
			}
		}
	}

	public void EndBatteryUsageImmediate()
	{
		if (numUsers <= 0)
		{
			numUsers = 0;

			StopCoroutine(rechargeCoroutine);
			rechargeCoroutine = RechargeBattery();
			StartCoroutine(rechargeCoroutine);
		}
	}

	public IEnumerator RechargeBattery()
	{
		if (maxBattery > 0f && rechargedPerSecond > 0f)
		{
			yield return new WaitForSeconds(timeUntilRecharge);

			while (currentBattery < maxBattery && rechargedPerSecond > 0f)
			{
				yield return new WaitForEndOfFrame();

				currentBattery += Time.deltaTime * rechargedPerSecond;

				if (currentBattery > maxBattery)
					currentBattery = maxBattery;

				UpdateCurrentBatteryHUD();
			}
		}
	}

	public void UpdateCurrentBatteryHUD()
	{
		if (currentBatteryField != null)
        {
            int newVal = Mathf.RoundToInt(currentBattery);

            if (newVal > 0)
                currentBatteryField.text = newVal.ToString();
            else
                currentBatteryField.text = "0";
        }
		

		if (batteryBar != null && batteryBarContainer != null)
		{
            float currentVal = currentBattery;

            if (currentVal < 0)
                currentVal = 0;

			if (maxBattery > 0f)
            {
                batteryBar.sizeDelta = new Vector2(Mathf.Lerp(0f, batteryBarContainer.rect.width, (currentVal / maxBattery)), batteryBar.rect.height);

                if (batteryBG != null)
                {
                    if (currentBattery < 0f)
                        batteryBG.color = negativeBatteryColor;
                    else
                        batteryBG.color = baseColor;
                }
            }
			else
				batteryBar.sizeDelta = new Vector2(0f, batteryBar.rect.height);
		}
	}

	public void SetMaxBattery(float newMax)
	{
		if (newMax >= 0f && newMax != maxBattery)
		{
			maxBattery = newMax;

			if (currentBattery > newMax)
				currentBattery = newMax;

			if (maxBatteryField != null)
				maxBatteryField.text = Mathf.RoundToInt(maxBattery).ToString();

			UpdateCurrentBatteryHUD();
		}
	}

	void OnDestroy()
	{
		if (primaryBatteryManager == this)
			primaryBatteryManager = null;
	}
}
