﻿using UnityEngine;
using System.Collections;

public class SpriteCamFollow : MonoBehaviour {

    public Camera mainCam;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        transform.position = mainCam.transform.position;
	
	}
}
